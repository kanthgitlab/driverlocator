package com.driverlocator.service;

import com.driverlocator.configuration.DriverLocatorIntegrationTestConfig;
import com.driverlocator.configuration.MongoTestConfig;
import com.driverlocator.entity.DriverGeo;
import com.driverlocator.entity.DriverGeoLog;
import com.driverlocator.model.DriverGeoLoggingResponseModel;
import com.driverlocator.model.DriverGeoModel;
import com.driverlocator.repository.DriverGeoLogRepository;
import com.driverlocator.repository.DriverGeoRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Test cases for the {@link DriverGeoServiceImpl}.
 *
 * @author lakshmikanth
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes ={MongoTestConfig.class, DriverLocatorIntegrationTestConfig.class})
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class DriverGeoServiceImplTest {

    Logger log = LoggerFactory.getLogger(DriverGeoServiceImplTest.class);

    @Autowired
    DriverGeoService driverGeoService;

    @Autowired
    DriverGeoRepository driverGeoRepository;

    @Autowired
    DriverGeoLogRepository driverGeoLogRepository;

    @Autowired
    ReactiveMongoTemplate mongoTemplate;

    @Before
    public void setUp() {
        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }

    @After
    public void cleanup() {
        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }

    @Test
    public void assert_DriverGeo_Log_Insertion(){

        //given

        cleanup();

        Long userId = Long.valueOf(2233);
        Double latitude = Double.valueOf(43.21);
        Double longitude = Double.valueOf(54.32);
        Double accuracy = Double.valueOf(100);

        DriverGeoModel driverGeoModel =
                getDriverGeoModelFromParameters(userId,latitude,longitude,accuracy);

        //when
        DriverGeoModel driverGeoModel_Saved =
                driverGeoService.insertDriverLocationLogEvent(driverGeoModel);


        //then
        assertNotNull(driverGeoModel_Saved);
        assertEquals(driverGeoModel,driverGeoModel_Saved);

        Flux<DriverGeoLog> driverGeoLogFlux = driverGeoLogRepository.getDriverGeoLogsByUserId(userId);

        List<DriverGeoLog> driverGeoLogList = driverGeoLogFlux.toStream().collect(Collectors.toList());

        assertNotNull(driverGeoLogList);
        assertEquals(1,driverGeoLogList.size());
        assertEquals(new GeoJsonPoint(longitude,latitude),driverGeoLogList.get(0).getLocation());
    }


    /**
     * Updates received after very first insertions, should be treated as
     * Update operation on Driver Details (DriverGeo)
     * Insert operation for audit purpose in to (DriverGeoLog)
     *
     * This test case covers this scenario
     */
    @Test
    public void assert_DriverGeo_Log_Update(){

        //given
        cleanup();

        Long userId = Long.valueOf(1233);
        Double latitude_1 = Double.valueOf(43.21);
        Double longitude_1 = Double.valueOf(54.32);
        Double accuracy_1 = Double.valueOf(100);

        DriverGeoModel driverGeoModel =
                getDriverGeoModelFromParameters(userId,latitude_1,longitude_1,accuracy_1);

        //when
        DriverGeoModel driverGeoModel_Saved =
                driverGeoService.insertDriverLocationLogEvent(driverGeoModel);

        //then
        assertNotNull(driverGeoModel_Saved);
        assertEquals(driverGeoModel,driverGeoModel_Saved);

        Flux<DriverGeoLog> driverGeoLogFlux = driverGeoLogRepository.getDriverGeoLogsByUserId(userId);

        List<DriverGeoLog> driverGeoLogList = driverGeoLogFlux.toStream().collect(Collectors.toList());

        assertNotNull(driverGeoLogList);
        assertEquals(1,driverGeoLogList.size());
        assertEquals(new GeoJsonPoint(longitude_1,latitude_1),driverGeoLogList.get(0).getLocation());

        Flux<DriverGeo> driverGeoFlux_Extracted = driverGeoRepository.findAllByUserId(userId);

        List<DriverGeo> driverGeoList = driverGeoFlux_Extracted.toStream().collect(Collectors.toList());

        assertNotNull(driverGeoList);
        assertEquals(1,driverGeoList.size());
        assertEquals(new GeoJsonPoint(longitude_1,latitude_1),driverGeoList.get(0).getLocation());

        //Update Event

        //Given
        Double latitude_2 = Double.valueOf(54.21);
        Double longitude_2 = Double.valueOf(67.32);
        Double accuracy_2 = Double.valueOf(80);


         driverGeoModel = getDriverGeoModelFromParameters(userId,latitude_2,longitude_2,accuracy_2);

        //when
         driverGeoModel_Saved =
                driverGeoService.insertDriverLocationLogEvent(driverGeoModel);

        //then
        assertNotNull(driverGeoModel_Saved);
        assertEquals(driverGeoModel,driverGeoModel_Saved);

         driverGeoLogFlux = driverGeoLogRepository.getDriverGeoLogsByUserId(userId);

         driverGeoLogList = driverGeoLogFlux.toStream().collect(Collectors.toList());

        assertNotNull(driverGeoLogList);
        assertEquals(2,driverGeoLogList.size());
        assertEquals(new GeoJsonPoint(longitude_1,latitude_1),driverGeoLogList.get(0).getLocation());
        assertEquals(new GeoJsonPoint(longitude_2,latitude_2),driverGeoLogList.get(1).getLocation());

        driverGeoFlux_Extracted = driverGeoRepository.findAllByUserId(userId);

        driverGeoList = driverGeoFlux_Extracted.toStream().collect(Collectors.toList());

        assertNotNull(driverGeoList);
        assertEquals(1,driverGeoList.size());
        assertEquals(new GeoJsonPoint(longitude_2,latitude_2),driverGeoList.get(0).getLocation());
    }


    @Test
    public void assert_get_DriverGeoLog_From_DriveGeoEvent(){

        //given

        cleanup();

        Long userId = Long.valueOf(1234);
        Double latitude = Double.valueOf(43.21);
        Double longitude = Double.valueOf(54.32);
        Double accuracy = Double.valueOf(100);

        DriverGeoModel driverGeoModel =
                getDriverGeoModelFromParameters(userId,latitude,longitude,accuracy);

        driverGeoService.insertDriverLocationLogEvent(driverGeoModel);

        DriverGeo driverGeo_SavedEvent = driverGeoRepository.checkDriverEntry(userId);

        //when
        DriverGeoLog driverGeoLog = DriverGeoServiceImpl.getDriverGeoLogFromDriveGeoEvent(driverGeo_SavedEvent);

        //then
        assertNotNull(driverGeoLog);
        assertEquals(userId,driverGeoLog.getUserId());
        assertNotNull(driverGeoLog.getId());
        assertEquals(driverGeo_SavedEvent.getLocation(),driverGeoLog.getLocation());
        assertEquals(driverGeo_SavedEvent.getLocationCoordinates(),driverGeoLog.getLocationCoordinates());
    }


    @Test
    public void assert_findDriverByUserId(){

        //given

        cleanup();

        Long userId = Long.valueOf(1235);
        Double latitude = Double.valueOf(43.21);
        Double longitude = Double.valueOf(54.32);
        Double accuracy = Double.valueOf(100);

        DriverGeoModel driverGeoModel =
                getDriverGeoModelFromParameters(userId,latitude,longitude,accuracy);

        driverGeoService.insertDriverLocationLogEvent(driverGeoModel);

        //when
        DriverGeoModel driverGeoModel_Extracted = driverGeoService.findDriverByUserId(userId);

        //then
        assertNotNull(driverGeoModel_Extracted);
        assertEquals(userId,driverGeoModel_Extracted.getUserId());
        assertEquals(latitude,driverGeoModel_Extracted.getLatitude());
        assertEquals(longitude,driverGeoModel_Extracted.getLongitude());
        assertEquals(accuracy,driverGeoModel_Extracted.getAccuracy());
    }

    @Test
    public void assert_Validate_Driver_Geo_Log_Details_To_True(){

        //given
        cleanup();

        Long userId = Long.valueOf(1236);
        Double latitude = Double.valueOf(43.21);
        Double longitude = Double.valueOf(54.32);
        Double accuracy = Double.valueOf(100);

        DriverGeoModel driverGeoModel =
                getDriverGeoModelFromParameters(userId,latitude,longitude,accuracy);


        //when
       DriverGeoLoggingResponseModel driverGeoLoggingResponseModel =
               driverGeoService.validateGeoLogEvent(driverGeoModel);


        //then
        assertNotNull(driverGeoLoggingResponseModel);
        assertTrue(driverGeoLoggingResponseModel.getIsValid());
        assertTrue(driverGeoLoggingResponseModel.getMessage().size()==0);
    }

    @Test
    public void assert_Validate_Driver_Geo_Log_Details_With_Invalid_DriverId(){

        //given
        cleanup();

        Long userId = Long.valueOf(50001);
        Double latitude = Double.valueOf(43.21);
        Double longitude = Double.valueOf(54.32);
        Double accuracy = Double.valueOf(100);

        DriverGeoModel driverGeoModel =
                getDriverGeoModelFromParameters(userId,latitude,longitude,accuracy);


        //when
        DriverGeoLoggingResponseModel driverGeoLoggingResponseModel =
                driverGeoService.validateGeoLogEvent(driverGeoModel);


        //then
        assertNotNull(driverGeoLoggingResponseModel);
        assertFalse(driverGeoLoggingResponseModel.getIsValid());
        assertTrue(driverGeoLoggingResponseModel.getMessage().size()==0);
    }

    @Test
    public void assert_Validate_Driver_Geo_Log_Details_With_Invalid_DriverId_And_Coordinates(){

        //given

        cleanup();

        Long userId = Long.valueOf(0);
        Double latitude = Double.valueOf(100.21);
        Double longitude = Double.valueOf(194.32);
        Double accuracy = Double.valueOf(100);

        DriverGeoModel driverGeoModel =
                getDriverGeoModelFromParameters(userId,latitude,longitude,accuracy);

        List<String> messages_Expected = Arrays.asList("Latitude should be between +/- 90","Longitude should be between +/- 180");


        //when
        DriverGeoLoggingResponseModel driverGeoLoggingResponseModel =
                driverGeoService.validateGeoLogEvent(driverGeoModel);


        //then
        assertNotNull(driverGeoLoggingResponseModel);
        assertFalse(driverGeoLoggingResponseModel.getIsValid());
        assertTrue(driverGeoLoggingResponseModel.getMessage().size()==2);
        assertEquals(messages_Expected,driverGeoLoggingResponseModel.getMessage());
    }




    public DriverGeoModel getDriverGeoModelFromParameters(Long userId,Double latitude,Double longitude,Double accuracy){
        return DriverGeoModel.builder().userId(userId).latitude(latitude).longitude(longitude).accuracy(accuracy).build();
    }




}
