package com.driverlocator.repository;

import com.driverlocator.configuration.MongoTestConfig;
import com.driverlocator.entity.DriverGeo;
import com.driverlocator.model.DriverLocatorQueryParameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

/**
 * Test cases for the {@link DriverGeoRepository}.
 *
 * @author lakshmikanth
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = MongoTestConfig.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class DriverGeoRepositoryImplTest {

    Logger log = LoggerFactory.getLogger(DriverGeoRepositoryImplTest.class);


    @Autowired
    DriverGeoRepository driverGeoRepository;

    @Autowired
    ReactiveMongoTemplate mongoTemplate;

    @Before
    public void setUp() {
        mongoTemplate.remove(DriverGeo.class);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void should_record_Drivers_GeoLog_Successfully() {

        // given

        Long userId  = new Long(1211);
        Double latitude = Double.valueOf(54.22) ;
        Double longitude = Double.valueOf(65.12) ;
        Double accuracy = Double.valueOf(100);


        DriverGeo  driverGeo =
                prepareDriverGeoEntityFromParameters(userId,latitude,longitude,accuracy);

        // when
        mongoTemplate.save(driverGeo).block();

        // then
        Flux<DriverGeo> drivers = mongoTemplate.findAll(DriverGeo.class);

        assertThat(drivers, notNullValue());

        assertEquals(Long.valueOf("1").longValue(), (drivers.collectList().block()).stream().count());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void should_extract_DriverGeo_Successfully() {

        // given

        Long userId  = new Long(1211);
        Double latitude = Double.valueOf(54.22) ;
        Double longitude = Double.valueOf(65.12) ;
        Double accuracy = Double.valueOf(100);


        DriverGeo  driverGeo =
                prepareDriverGeoEntityFromParameters(userId,latitude,longitude,accuracy);

        DriverGeo driverGeo_Saved_Actual =mongoTemplate.save(driverGeo).block();

        // when
        DriverGeo driverGeo_Extracted_Actual = driverGeoRepository.checkDriverEntry(userId);

        // then
        assertThat(driverGeo_Extracted_Actual, notNullValue());
        assertEquals(driverGeo_Saved_Actual,driverGeo_Extracted_Actual);
        assertNotNull(driverGeo_Extracted_Actual.getId());
    }


    /**
     *  Scenario:
     *
     *  Given:
     *
     *  6 drivers with-in proximity of 9 kilometers:
     *
     *  1st at a distance of: 0 Kilometers with locationCoordinates=[65.12, 54.22]
     *  2nd at a distance of : 3.87 Kilometers with locationCoordinates=[65.09, 54.19]
     *  3rd at a distance of : 3.94 Kilometers with locationCoordinates=[65.07, 54.2]
     *  4th at a distance of : 4.63 Kilometers with locationCoordinates=[65.1, 54.18]
     *  5th at a distance of : 8.44 Kilometers with locationCoordinates=[65.07, 54.15]
     *  6th at a distance of : 9.03 Kilometers with locationCoordinates=[65.05, 54.15]
     *
     *  when:
     *  Customer is located at co-ordinates: latitude: 54.22, longitude: 65.12
     *  and Search of drivers with in the radius of 9 kilometers
     *
     *  Then:
     *  Assert that NearGeo (GeoSpatial Search) returns 5 drivers
     *  Each with a distance in the range of 0 to 9 kilometers of proximity
     *
     *  ** distance is available in the response Object.
     *
     */
    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void should_find_All_Drivers_WithIn_Proximity_Successfully() {

        // given
        prepare_For_Driver_Search_Test();

        Double latitude_Search_Parameter = Double.valueOf(54.22);
        Double longitude_Search_Parameter = Double.valueOf(65.12);
        Long radius_Search_Parameter = Long.valueOf(9000);
        Integer limit_Search_Parameter = 100;

        DriverLocatorQueryParameters  driverLocatorQueryParameters = DriverLocatorQueryParameters.builder()
                .latitude(latitude_Search_Parameter).longitude(longitude_Search_Parameter)
                .radius(radius_Search_Parameter).limit(limit_Search_Parameter).build();

        log.info("drivers saved for test: {} ",mongoTemplate.findAll(DriverGeo.class).toStream().collect(Collectors.toList()));

        // when
        List<DriverGeo> drivers_In_Proximity_Actual =
                driverGeoRepository.findNearestDriversWithInRadius(driverLocatorQueryParameters);

        log.info("drivers with-in proximity of 10 kilometers: {}",drivers_In_Proximity_Actual);

        // then
        assertNotNull(drivers_In_Proximity_Actual);
        assertEquals(5,drivers_In_Proximity_Actual.size());

    }


    /**
     *  Scenario:
     *
     *  Given:
     *
     *  6 drivers with-in proximity of 10 kilometers:
     *
     *  1st at a distance of: 0 Kilometers with locationCoordinates=[65.12, 54.22]
     *  2nd at a distance of : 3.87 Kilometers with locationCoordinates=[65.09, 54.19]
     *  3rd at a distance of : 3.94 Kilometers with locationCoordinates=[65.07, 54.2]
     *  4th at a distance of : 4.63 Kilometers with locationCoordinates=[65.1, 54.18]
     *  5th at a distance of : 8.44 Kilometers with locationCoordinates=[65.07, 54.15]
     *  6th at a distance of : 9.03 Kilometers with locationCoordinates=[65.05, 54.15]
     *
     *  when:
     *  Customer is located at co-ordinates: latitude: 54.22, longitude: 65.12
     *  and Search of drivers with in the radius of 4 kilometers
     *
     *  Then:
     *  Assert that NearGeo (GeoSpatial Search) returns 3 drivers
     *  Each with a distance in the range of 0 to 4 kilometers of proximity
     *
     *  ** distance is available in the response Object.
     *
     */
    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void should_find_All_Drivers_WithIn_Closer_Proximity_Successfully() {

        // given
        prepare_For_Driver_Search_Test();

        Double latitude_Search_Parameter = Double.valueOf(54.22);
        Double longitude_Search_Parameter = Double.valueOf(65.12);
        Long radius_Search_Parameter = Long.valueOf(4000);
        Integer limit_Search_Parameter = 100;

        DriverLocatorQueryParameters  driverLocatorQueryParameters = DriverLocatorQueryParameters.builder()
                .latitude(latitude_Search_Parameter).longitude(longitude_Search_Parameter)
                .radius(radius_Search_Parameter).limit(limit_Search_Parameter).build();

        log.info("drivers saved for test: {} ",mongoTemplate.findAll(DriverGeo.class).toStream().collect(Collectors.toList()));

        // when
        List<DriverGeo> drivers_In_Proximity_Actual =
                driverGeoRepository.findNearestDriversWithInRadius(driverLocatorQueryParameters);

        log.info("drivers with-in proximity of 10 kilometers: {}",drivers_In_Proximity_Actual);

        // then
        assertNotNull(drivers_In_Proximity_Actual);
        assertEquals(3,drivers_In_Proximity_Actual.size());

    }

    /**
     *  Scenario:
     *
     *  Given:
     *
     *  6 drivers with-in proximity of 10 kilometers:
     *
     *  1st at a distance of: 0 Kilometers with locationCoordinates=[65.12, 54.22]
     *  2nd at a distance of : 3.87 Kilometers with locationCoordinates=[65.09, 54.19]
     *  3rd at a distance of : 3.94 Kilometers with locationCoordinates=[65.07, 54.2]
     *  4th at a distance of : 4.63 Kilometers with locationCoordinates=[65.1, 54.18]
     *  5th at a distance of : 8.44 Kilometers with locationCoordinates=[65.07, 54.15]
     *  6th at a distance of : 9.03 Kilometers with locationCoordinates=[65.05, 54.15]
     *
     *  when:
     *  Customer is located at co-ordinates: latitude: 54.22, longitude: 65.12
     *  and Search of drivers with in the radius of 100 meters
     *
     *  Then:
     *  Assert that NearGeo (GeoSpatial Search) returns 1 driver
     *  Each with a distance in the range of 0 to 100 meters of proximity
     *
     *  ** distance is available in the response Object.
     *
     */
    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void should_Assert_Behavior_For_SuperCloser_Proximity() {

        // given
        prepare_For_Driver_Search_Test();

        Double latitude_Search_Parameter = Double.valueOf(54.22);
        Double longitude_Search_Parameter = Double.valueOf(65.12);
        Long radius_Search_Parameter = Long.valueOf(100);
        Integer limit_Search_Parameter = 100;

        DriverLocatorQueryParameters  driverLocatorQueryParameters = DriverLocatorQueryParameters.builder()
                .latitude(latitude_Search_Parameter).longitude(longitude_Search_Parameter)
                .radius(radius_Search_Parameter).limit(limit_Search_Parameter).build();

        log.info("drivers saved for test: {} ",mongoTemplate.findAll(DriverGeo.class).toStream().collect(Collectors.toList()));

        // when
        List<DriverGeo> drivers_In_Proximity_Actual =
                driverGeoRepository.findNearestDriversWithInRadius(driverLocatorQueryParameters);

        log.info("drivers with-in proximity of 10 kilometers: {}",drivers_In_Proximity_Actual);

        // then
        assertNotNull(drivers_In_Proximity_Actual);
        assertEquals(1,drivers_In_Proximity_Actual.size());

    }




    public List<DriverGeo> prepare_For_Driver_Search_Test(){

        List<DriverGeo> driverGeos = new ArrayList<>();

        Long userId_1  = new Long(1211);
        Double latitude_1 = Double.valueOf(54.22) ;
        Double longitude_1 = Double.valueOf(65.12) ;
        Double accuracy_1 = Double.valueOf(100);

        DriverGeo  driverGeo_1 =save_DriverGeo_TestData(userId_1,latitude_1,longitude_1,accuracy_1);

        driverGeos.add(driverGeo_1);

        Long userId_2  = new Long(3999);
        Double latitude_2 = Double.valueOf(54.20) ;
        Double longitude_2 = Double.valueOf(65.07) ;
        Double accuracy_2 = Double.valueOf(100);

        DriverGeo  driverGeo_2 =save_DriverGeo_TestData(userId_2,latitude_2,longitude_2,accuracy_2);

        driverGeos.add(driverGeo_2);

        Long userId_3  = new Long(42000);
        Double latitude_3 = Double.valueOf(54.19) ;
        Double longitude_3 = Double.valueOf(65.09) ;
        Double accuracy_3 = Double.valueOf(100);

        DriverGeo  driverGeo_3 =save_DriverGeo_TestData(userId_3,latitude_3,longitude_3,accuracy_3);

        driverGeos.add(driverGeo_3);

        Long userId_4  = new Long(333);
        Double latitude_4 = Double.valueOf(54.18) ;
        Double longitude_4 = Double.valueOf(65.10) ;
        Double accuracy_4 = Double.valueOf(100);

        DriverGeo  driverGeo_4 =save_DriverGeo_TestData(userId_4,latitude_4,longitude_4,accuracy_4);

        driverGeos.add(driverGeo_4);

        Long userId_5  = new Long(8765);
        Double latitude_5 = Double.valueOf(54.15) ;
        Double longitude_5 = Double.valueOf(65.07) ;
        Double accuracy_5 = Double.valueOf(100);

        DriverGeo  driverGeo_5 =save_DriverGeo_TestData(userId_5,latitude_5,longitude_5,accuracy_5);

        driverGeos.add(driverGeo_5);

        Long userId_6  = new Long(8799);
        Double latitude_6 = Double.valueOf(54.15) ;
        Double longitude_6 = Double.valueOf(65.05) ;
        Double accuracy_6 = Double.valueOf(100);

        DriverGeo  driverGeo_6 =save_DriverGeo_TestData(userId_6,latitude_6,longitude_6,accuracy_6);

        driverGeos.add(driverGeo_6);

        return driverGeos;
    }

    private DriverGeo save_DriverGeo_TestData(Long userId,Double latitude,Double longitude,Double accuracy){

        DriverGeo driverGeo = prepareDriverGeoEntityFromParameters(userId, latitude, longitude, accuracy);

        return mongoTemplate.save(driverGeo).block();
    }

    private DriverGeo prepareDriverGeoEntityFromParameters(Long userId, Double latitude, Double longitude, Double accuracy) {
        Double[] locationCordinates = new Double[2];

        locationCordinates[0] = longitude;
        locationCordinates[1] = latitude;

        return DriverGeo.builder().userId(userId).latitude(latitude)
                .longitude(longitude).accuracy(accuracy).location(new GeoJsonPoint(longitude,latitude))
                .locationCoordinates(locationCordinates).build();
    }


}
