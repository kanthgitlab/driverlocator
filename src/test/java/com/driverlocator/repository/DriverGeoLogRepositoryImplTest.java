package com.driverlocator.repository;

import com.driverlocator.configuration.MongoTestConfig;
import com.driverlocator.entity.DriverGeoLog;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Test cases for the {@link DriverGeoLogRepository}.
 *
 * @author lakshmikanth
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = MongoTestConfig.class)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class DriverGeoLogRepositoryImplTest {

    Logger log = LoggerFactory.getLogger(DriverGeoLogRepositoryImplTest.class);


    @Autowired
    DriverGeoLogRepository driverGeoLogRepository;

    @Autowired
    ReactiveMongoTemplate mongoTemplate;

    @Before
    public void setUp() {
        mongoTemplate.remove(DriverGeoLog.class);
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void should_record_Driver_GeoLogs_Successfully() {

        // given

        Long userId  = new Long(1211);
        Double latitude = Double.valueOf(54.22) ;
        Double longitude = Double.valueOf(65.12) ;
        Double accuracy = Double.valueOf(100);


        DriverGeoLog  driverGeoLog =
                prepareDriverGeoLogEntityFromParameters(userId,latitude,longitude,accuracy);

        // when
        mongoTemplate.save(driverGeoLog).block();

        // then
        Flux<DriverGeoLog> drivers = mongoTemplate.findAll(DriverGeoLog.class);

        assertThat(drivers, notNullValue());

        assertEquals(Long.valueOf("1").longValue(), (drivers.collectList().block()).stream().count());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    public void should_extract_DriverGeoLogs_Successfully() {

        // given

        Long userId  = new Long(1211);
        Double latitude = Double.valueOf(54.22) ;
        Double longitude = Double.valueOf(65.12) ;
        Double accuracy = Double.valueOf(100);


        DriverGeoLog  driverGeoLog =
                prepareDriverGeoLogEntityFromParameters(userId,latitude,longitude,accuracy);

        DriverGeoLog driverGeoLog_Saved_Actual =mongoTemplate.save(driverGeoLog).block();


        Double latitude1 = Double.valueOf(55.22) ;
        Double longitude1 = Double.valueOf(69.12) ;
        Double accuracy1 = Double.valueOf(100);


        DriverGeoLog  driverGeoLog1 =
                prepareDriverGeoLogEntityFromParameters(userId,latitude1,longitude1,accuracy1);

        DriverGeoLog driverGeoLog_Saved_Actual1 =mongoTemplate.save(driverGeoLog1).block();

        // when
        List<DriverGeoLog> driverGeoLogs_Extracted_Actual =
                driverGeoLogRepository.getDriverGeoLogsByUserId(userId).toStream().collect(Collectors.toList());

        // then
        assertThat(driverGeoLogs_Extracted_Actual, notNullValue());
        assertEquals(2,driverGeoLogs_Extracted_Actual.size());
        assertEquals(Lists.newArrayList(driverGeoLog_Saved_Actual,driverGeoLog_Saved_Actual1),driverGeoLogs_Extracted_Actual);
    }



    private DriverGeoLog save_DriverGeoLog_TestData(Long userId,Double latitude,Double longitude,Double accuracy){

        DriverGeoLog driverGeoLog = prepareDriverGeoLogEntityFromParameters(userId, latitude, longitude, accuracy);

        return mongoTemplate.save(driverGeoLog).block();
    }

    private DriverGeoLog prepareDriverGeoLogEntityFromParameters(Long userId, Double latitude, Double longitude, Double accuracy) {
        Double[] locationCordinates = new Double[2];

        locationCordinates[0] = longitude;
        locationCordinates[1] = latitude;

        return DriverGeoLog.builder().userId(userId).latitude(latitude)
                .longitude(longitude).accuracy(accuracy).location(new GeoJsonPoint(longitude,latitude))
                .locationCoordinates(locationCordinates).build();
    }


}
