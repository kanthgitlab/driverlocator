package com.driverlocator;

import com.driverlocator.configuration.DriverLocatorIntegrationTestConfig;
import com.driverlocator.configuration.MongoTestConfig;
import com.driverlocator.model.ErrorModel;
import com.driverlocator.repository.DriverGeoLogRepository;
import com.driverlocator.repository.DriverGeoRepository;
import com.driverlocator.rest.DriverGeoAsyncController;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


/**
 * Test cases for the {@link com.driverlocator.rest.DriverGeoAsyncController}.
 *
 * @author lakshmikanth
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes ={MongoTestConfig.class, DriverLocatorIntegrationTestConfig.class})
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class DriverLocationPersistenceIntegrationTest {


    Logger log = LoggerFactory.getLogger(DriverLocationPersistenceIntegrationTest.class);

    MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    DriverGeoAsyncController driverGeoAsyncController;

    @Autowired
    DriverGeoLogRepository driverGeoLogRepository;


    @Autowired
    DriverGeoRepository driverGeoRepository;


    ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() throws Exception {

        // Standalone context
        this.mockMvc = standaloneSetup(this.driverGeoAsyncController).build();

        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }



    @After
    public void cleanup() {
        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }

    @Test
    public void test_record_Driver_GeoLog_Successfully() throws Exception {

        MvcResult mvcResult = mockMvc.perform(put("/drivers/{id}/location","1121")
                .contentType(APPLICATION_JSON)
                .content("{\"latitude\": 12.97161923, \"longitude\": 77.59463452, \"accuracy\": 0.7\n}")
                .accept(APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andReturn();

        Object responseObject = mvcResult.getAsyncResult();

        //then
        assertNotNull(responseObject);
        assertTrue(responseObject instanceof ResponseEntity);

        ResponseEntity responseEntity_Actual = (ResponseEntity)responseObject;

        assertTrue(responseEntity_Actual.getStatusCode().equals(HttpStatus.OK));
        assertTrue(responseEntity_Actual.getBody().equals("{}"));
    }

    @Test
    public void test_record_Driver_GeoLog_With_Validation_With_Invalid_Coordinates() throws Exception {

        MvcResult mvcResult = mockMvc.perform(put("/drivers/{id}/location","9121")
                .contentType(APPLICATION_JSON)
                .content("{\"latitude\": 212.97161923, \"longitude\": 477.59463452, \"accuracy\": 0.7\n}")
                .accept(APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andReturn();

        Object responseObject = mvcResult.getAsyncResult();

        //then
        assertNotNull(responseObject);
        assertTrue(responseObject instanceof ResponseEntity);

        ResponseEntity responseEntity_Actual = (ResponseEntity)responseObject;

        assertTrue(responseEntity_Actual.getStatusCode().equals(HttpStatus.UNPROCESSABLE_ENTITY));

        ErrorModel errorModel = (ErrorModel) responseEntity_Actual.getBody();

        assertEquals("{\"errors\":[\"Latitude should be between +/- 90\",\"Longitude should be between +/- 180\"]}"
                ,objectMapper.writeValueAsString(errorModel));
    }

    @Test
    public void test_record_Driver_GeoLog_With_Invalid_DriverId() throws Exception {


        MvcResult mvcResult = mockMvc.perform(put("/drivers/{id}/location","59121")
                .contentType(APPLICATION_JSON)
                .content("{\"latitude\": 212.97161923, \"longitude\": 477.59463452, \"accuracy\": 0.7\n}")
                .accept(APPLICATION_JSON))
                .andExpect(request().asyncStarted())
                .andReturn();

        Object responseObject = mvcResult.getAsyncResult();

        //then
        assertNotNull(responseObject);
        assertTrue(responseObject instanceof ResponseEntity);

        ResponseEntity responseEntity_Actual = (ResponseEntity)responseObject;

        assertTrue(responseEntity_Actual.getStatusCode().equals(HttpStatus.NOT_FOUND));
        assertTrue(responseEntity_Actual.getBody().equals("{}"));

    }



}
