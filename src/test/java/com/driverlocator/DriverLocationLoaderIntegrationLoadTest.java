package com.driverlocator;

import com.driverlocator.common.DateTimeUtil;
import com.driverlocator.configuration.DriverLocatorIntegrationTestConfig;
import com.driverlocator.configuration.ModelMapperBean;
import com.driverlocator.configuration.MongoTestConfig;
import com.driverlocator.model.DriverGeoModel;
import com.driverlocator.repository.DriverGeoLogRepository;
import com.driverlocator.repository.DriverGeoRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoReactiveAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@ContextConfiguration(classes = {MongoTestConfig.class, DriverLocatorIntegrationTestConfig.class})
@RunWith(SpringRunner.class)
@PropertySource("classpath:application.yml")
@AutoConfigureBefore({EmbeddedMongoAutoConfiguration.class, ModelMapperBean.class})
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class, MongoReactiveAutoConfiguration.class, MongoDataAutoConfiguration.class})
@SpringBootTest(classes = {DriverLocatorApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class DriverLocationLoaderIntegrationLoadTest {

    Logger log = LoggerFactory.getLogger(DriverLocationLoaderIntegrationLoadTest.class);

    @LocalServerPort
    private int serverport;

    private String url;

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    DriverGeoLogRepository driverGeoLogRepository;

    @Autowired
    DriverGeoRepository driverGeoRepository;

    @Before
    public void setUp() throws Exception{
        url="http://localhost:"+serverport;

        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }

    @After
    public void cleanup() {
        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }

    List<Double> locationCoordinates = Arrays.asList(32.12,76.54,12.21,78.90,37.11,65.11,19.11,86.11,34.11,55.11);

    public Double getRandomCoordinateInValidRange() {
        int index = ThreadLocalRandom.current().nextInt(locationCoordinates.size());
        return locationCoordinates.get(index);
    }

    List<Integer> driverIds = IntStream.range(1,50001).boxed().collect(Collectors.toList());

    public Integer getRandomDriverIdInValidRange(){
        int index = ThreadLocalRandom.current().nextInt(driverIds.size());
        return driverIds.get(index);
    }

    List<Integer> delay_List = IntStream.range(1,3).boxed().collect(Collectors.toList());

    public Integer getRandomDelayInValidRange(){
        int index = ThreadLocalRandom.current().nextInt(delay_List.size());
        return delay_List.get(index);
    }


    @Test
    public void test_DriverLocatorAPI_OnLoad() throws Exception{

        LocalDateTime startTime = DateTimeUtil.getCurrentTimeStamp();

        log.info("load test on 1000 count - initiated: {}", startTime);

        IntStream.range(1,1000).forEach(new IntConsumer() {
            @Override
            public void accept(int logId) {

                try {
                    insert_Test_Data(String.valueOf(getRandomDriverIdInValidRange()),getRandomCoordinateInValidRange().toString(),
                                                    getRandomCoordinateInValidRange().toString(),"100");

                    log.info("logging for Id: {}",logId);
                } catch (Exception exc) {
                    log.error("exception while inserting test data ",exc);
                }

            }
        });

        log.info("load test on 1000 count - ended: {} with startTime: {}", DateTimeUtil.getCurrentTimeStamp(),startTime);
    }


    public void insert_Test_Data(String id,String latitude,String longitude, String accuracy) throws Exception{

        final String recordDriverLocationURL = String.format("%s/drivers/{id}/location",url);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        Map<String, String> param = new HashMap<String, String>();
        param.put("id",id);

        DriverGeoModel driverGeoModel = DriverGeoModel.builder().latitude(Double.valueOf(latitude))
                .longitude(Double.valueOf(longitude)).accuracy(Double.valueOf(accuracy)).build();

        HttpHeaders headers = new HttpHeaders();

        HttpEntity<DriverGeoModel> requestEntity = new HttpEntity<DriverGeoModel>(driverGeoModel, headers);

        HttpEntity<Object> response = restTemplate.exchange(recordDriverLocationURL, HttpMethod.PUT, requestEntity,Object.class,  param);

        log.info("response body: {}",response.getBody());
    }


}
