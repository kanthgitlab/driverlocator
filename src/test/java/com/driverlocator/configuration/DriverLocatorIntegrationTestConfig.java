package com.driverlocator.configuration;

import com.driverlocator.repository.DriverGeoLogRepository;
import com.driverlocator.repository.DriverGeoRepository;
import com.driverlocator.rest.DriverFinderAsyncController;
import com.driverlocator.rest.DriverGeoAsyncController;
import com.driverlocator.service.DriverFinderService;
import com.driverlocator.service.DriverFinderServiceImpl;
import com.driverlocator.service.DriverGeoService;
import com.driverlocator.service.DriverGeoServiceImpl;
import org.modelmapper.ModelMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
@AutoConfigureBefore({MongoTestConfig.class,DriverLocatorAsycConfigurer.class})
@EnableAutoConfiguration(exclude = {
        MongoAutoConfiguration.class,
        MongoDataAutoConfiguration.class})

public class DriverLocatorIntegrationTestConfig {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DriverLocatorIntegrationTestConfig.class);

    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }


    @Bean
    public DriverGeoService driverGeoService(DriverGeoRepository driverGeoRepository, DriverGeoLogRepository driverGeoLogRepository, ModelMapper modelMapper){

        return new DriverGeoServiceImpl(driverGeoRepository,driverGeoLogRepository,modelMapper);

    }

    @Bean
    public DriverFinderService driverFinderService( DriverGeoRepository driverGeoRepository, ModelMapper modelMapper){

        return new DriverFinderServiceImpl(driverGeoRepository,modelMapper);

    }

    @Bean
    @Qualifier("asyncTestTaskExecutor")
    ThreadPoolTaskExecutor threadPoolTaskExecutor() {

        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(1000);
        threadPoolTaskExecutor.setMaxPoolSize(5000);
        threadPoolTaskExecutor.setQueueCapacity(10000);
        threadPoolTaskExecutor.setKeepAliveSeconds(1000);
        threadPoolTaskExecutor.setThreadNamePrefix("DriverLocatorTestAsyncProcessor-");
        threadPoolTaskExecutor.initialize();

        return threadPoolTaskExecutor;
    }


    @Bean
    //@Qualifier("asyncTaskExecutor")
    WebMvcConfigurer configurer(ThreadPoolTaskExecutor asyncTestTaskExecutor) {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void configureAsyncSupport(AsyncSupportConfigurer configurer) {

                configurer.setTaskExecutor(asyncTestTaskExecutor);
            }
        };
    }


    @Bean
    public DriverGeoAsyncController driverGeoAsyncController(DriverGeoService driverGeoService,ThreadPoolTaskExecutor asyncTestTaskExecutor){

        return new DriverGeoAsyncController(driverGeoService,asyncTestTaskExecutor);

    }

    @Bean
    public DriverFinderAsyncController driverFinderAsyncController(DriverFinderService driverFinderService,ThreadPoolTaskExecutor asyncTestTaskExecutor){

        return new DriverFinderAsyncController(driverFinderService,asyncTestTaskExecutor);

    }


}
