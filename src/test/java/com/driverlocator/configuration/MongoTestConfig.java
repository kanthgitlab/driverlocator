package com.driverlocator.configuration;

import com.driverlocator.repository.DriverGeoLogRepositoryImpl;
import com.driverlocator.repository.DriverGeoRepositoryImpl;
import com.mongodb.MongoCredential;
import com.mongodb.reactivestreams.client.MongoClients;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import javax.annotation.PreDestroy;
import java.io.IOException;


@Configuration
@AutoConfigureBefore({EmbeddedMongoAutoConfiguration.class,ModelMapperBean.class})
@EnableAutoConfiguration(exclude = {
        MongoAutoConfiguration.class,
        MongoDataAutoConfiguration.class})
@ConditionalOnProperty(value="appMode", havingValue="test")
public class MongoTestConfig extends AbstractReactiveMongoConfiguration{

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MongoTestConfig.class);

    private static final String MONGO_DB_URL = "localhost";
    private static final Integer MONGO_DB_PORT = 27018;
    private static final String MONGO_DB_NAME = "embeded_db";
    private MongodExecutable mongodExecutable;
    private MongoTemplate mongoTemplate;

    /**
     * Return the Reactive Streams {@link com.mongodb.reactivestreams.client.MongoClient} instance to connect to. Annotate with {@link Bean} in case you want
     * to expose a {@link com.mongodb.reactivestreams.client.MongoClient} instance to the {@link ApplicationContext}.
     *
     * @return never {@literal null}.
     */
    @Bean
    public com.mongodb.reactivestreams.client.MongoClient reactiveMongoClient() {

       startMongod();

        //mongoTemplate = new MongoTemplate(new MongoClient(ip, port), "test");

        return MongoClients.create(String.format("mongodb://localhost:%d", MONGO_DB_PORT));

    }

    private void startMongod() {
        String ip = "localhost";
        int port = 27018;

        IMongodConfig mongodConfig = null;
        try {
            mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
                    .net(new Net(ip, port, Network.localhostIsIPv6()))
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MongodStarter starter = MongodStarter.getDefaultInstance();
        mongodExecutable = starter.prepare(mongodConfig);
        try {
            mongodExecutable.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PreDestroy
    public void stopMongod() throws IOException {
      mongodExecutable.stop();
    }

    @Bean
    public ReactiveMongoTemplate mongoTemplate() throws Exception {
        return new ReactiveMongoTemplate(reactiveMongoClient(), MONGO_DB_NAME);
    }


    @Bean
    public MongoCredential mongoCredentials() throws Exception {
        return MongoCredential.createCredential("", MONGO_DB_NAME, "".toCharArray());
    }


    /**
     * mandatory method to be overridden for GRIDFS Template
     * @return
     */
    @Override
    protected String getDatabaseName() {
        return MONGO_DB_NAME;
    }

    @Bean
    public DriverGeoRepositoryImpl driverGeoRepository(){
        return new DriverGeoRepositoryImpl();
    }


    @Bean
    public DriverGeoLogRepositoryImpl driverGeoLogRepository(){
        return new DriverGeoLogRepositoryImpl();
    }

}
