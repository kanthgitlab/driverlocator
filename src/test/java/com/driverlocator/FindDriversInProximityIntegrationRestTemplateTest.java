package com.driverlocator;

import com.driverlocator.configuration.DriverLocatorIntegrationTestConfig;
import com.driverlocator.configuration.ModelMapperBean;
import com.driverlocator.configuration.MongoTestConfig;
import com.driverlocator.model.DriverGeoModel;
import com.driverlocator.model.DriverLocatorModel;
import com.driverlocator.repository.DriverGeoLogRepository;
import com.driverlocator.repository.DriverGeoRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoReactiveAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;



@ContextConfiguration(classes = {MongoTestConfig.class,DriverLocatorIntegrationTestConfig.class})
@RunWith(SpringRunner.class)
@PropertySource("classpath:application.yml")
@AutoConfigureBefore({EmbeddedMongoAutoConfiguration.class, ModelMapperBean.class})
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class, MongoReactiveAutoConfiguration.class, MongoDataAutoConfiguration.class})
@SpringBootTest(classes = {DriverLocatorApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class FindDriversInProximityIntegrationRestTemplateTest {

    Logger log = LoggerFactory.getLogger(FindDriversInProximityIntegrationRestTemplateTest.class);

    @LocalServerPort
    private int serverport;
    private String url;

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    DriverGeoLogRepository driverGeoLogRepository;


    @Autowired
    DriverGeoRepository driverGeoRepository;


    @Before
    public void setUp() throws Exception{
        url="http://localhost:"+serverport;

        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }



    @After
    public void cleanup() {
        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }

    /**
     *  Scenario:
     *
     *  Given:
     *
     *  6 drivers with-in proximity of 9 kilometers:
     *
     *  1st at a distance of: 0 Kilometers with locationCoordinates=[65.12, 54.22]
     *  2nd at a distance of : 3.87 Kilometers with locationCoordinates=[65.09, 54.19]
     *  3rd at a distance of : 3.94 Kilometers with locationCoordinates=[65.07, 54.2]
     *  4th at a distance of : 4.63 Kilometers with locationCoordinates=[65.1, 54.18]
     *  5th at a distance of : 8.44 Kilometers with locationCoordinates=[65.07, 54.15]
     *  6th at a distance of : 9.03 Kilometers with locationCoordinates=[65.05, 54.15]
     *
     *  when:
     *  Customer is located at co-ordinates: latitude: 54.22, longitude: 65.12
     *  and Search of drivers with in the radius of 9 kilometers
     *
     *  Then:
     *  Assert that NearGeo (GeoSpatial Search) returns 5 drivers
     *  Each with a distance in the range of 0 to 9 kilometers of proximity
     *
     *  ** distance is available in the response Object.
     *
     */
    @Test
    public void test_find_All_Driver_GeoLogs_In_Proximity_Successfully() throws Exception {


        String userId_1  = "1211";
        String latitude_1 = "54.22" ;
        String longitude_1 = "65.12" ;
        String accuracy_1 = "100";

        insert_Test_Data(userId_1,latitude_1,longitude_1,accuracy_1);

        String userId_2  = "3999";
        String latitude_2 = "54.20" ;
        String longitude_2 = "65.07" ;
        String accuracy_2 = "100";

        insert_Test_Data(userId_2,latitude_2,longitude_2,accuracy_2);


        String userId_3  = "42000";
        String latitude_3 = "54.19";
        String longitude_3 = "65.09";
        String accuracy_3 = "100";

        insert_Test_Data(userId_3,latitude_3,longitude_3,accuracy_3);

        String userId_4  = "333";
        String latitude_4 = "54.18";
        String longitude_4 = "65.10";
        String accuracy_4 = "100";

        insert_Test_Data(userId_4,latitude_4,longitude_4,accuracy_4);

        String userId_5  = "8765";
        String latitude_5 = "54.15";
        String longitude_5 = "65.07";
        String accuracy_5 = "100";

        insert_Test_Data(userId_5,latitude_5,longitude_5,accuracy_5);

        String userId_6  = "8799";
        String latitude_6 = "54.15";
        String longitude_6 = "65.05";
        String accuracy_6 = "100";

        insert_Test_Data(userId_6,latitude_6,longitude_6,accuracy_6);


        final String getDriversNearByLocationURL = String.format("%s/drivers",url);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        String latitude = "54.22";
        String longitude = "65.12";
        String radius = "9000";
        String limit = "10";

        Map<String, String> params = new HashMap<String, String>();
        params.put("latitude",latitude);
        params.put("longitude",longitude);
        params.put("radius",radius);
        params.put("limit",limit);


        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getDriversNearByLocationURL)
                .queryParam("latitude", latitude)
                .queryParam("longitude", longitude)
                .queryParam("radius", radius)
                .queryParam("limit", limit);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<Object> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                Object.class);

        log.info("responseEntity Type: {}",response.getBody().getClass());


        //then
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertTrue(response.getBody() instanceof List);

        List<DriverLocatorModel> driverLocatorModels = (List<DriverLocatorModel>) response.getBody();

        assertTrue(driverLocatorModels.size()==5);
  }




    public void insert_Test_Data(String id,String latitude,String longitude, String accuracy) throws Exception{

        final String recordDriverLocationURL = String.format("%s/drivers/{id}/location",url);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        Map<String, String> param = new HashMap<String, String>();
        param.put("id",id);

        DriverGeoModel driverGeoModel = DriverGeoModel.builder().latitude(Double.valueOf(latitude))
                .longitude(Double.valueOf(longitude)).accuracy(Double.valueOf(accuracy)).build();

        HttpHeaders headers = new HttpHeaders();

        HttpEntity<DriverGeoModel> requestEntity = new HttpEntity<DriverGeoModel>(driverGeoModel, headers);

        HttpEntity<Object>  response = restTemplate.exchange(recordDriverLocationURL, HttpMethod.PUT, requestEntity,Object.class,  param);

        log.info("response body: {}",response.getBody());
    }
}
