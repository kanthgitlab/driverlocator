package com.driverlocator;

import com.driverlocator.configuration.DriverLocatorIntegrationTestConfig;
import com.driverlocator.configuration.MongoTestConfig;
import com.driverlocator.entity.DriverGeo;
import com.driverlocator.model.DriverLocatorModel;
import com.driverlocator.model.ErrorModel;
import com.driverlocator.repository.DriverGeoLogRepository;
import com.driverlocator.repository.DriverGeoRepository;
import com.driverlocator.rest.DriverFinderAsyncController;
import com.driverlocator.rest.DriverGeoAsyncController;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.hamcrest.Matchers.anything;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


/**
 * Test cases for the {@link DriverGeoAsyncController}.
 *
 * @author lakshmikanth
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes ={MongoTestConfig.class, DriverLocatorIntegrationTestConfig.class})
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class FindDriversInProximityIntegrationTest {


    Logger log = LoggerFactory.getLogger(FindDriversInProximityIntegrationTest.class);

    MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    DriverFinderAsyncController driverFinderAsyncController;

    @Autowired
    DriverGeoAsyncController driverGeoAsyncController;

    @Autowired
    DriverGeoRepository driverGeoRepository;

    @Autowired
    DriverGeoLogRepository  driverGeoLogRepository;

    @Autowired
    ReactiveMongoTemplate mongoTemplate;

    ObjectMapper objectMapper = new ObjectMapper();


    @Before
    public void setup() throws Exception {

        // Standalone context
        this.mockMvc = standaloneSetup(this.driverFinderAsyncController,this.driverGeoAsyncController).build();

        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
        objectMapper.enable(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN);
    }


    @After
    public void cleanup() {
        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }

    protected ResultActions performTest(MockHttpServletRequestBuilder builder) throws Exception {
        ResultActions resultActions = mockMvc.perform(builder);
        if (resultActions.andReturn().getRequest().isAsyncStarted()) {
            return mockMvc.perform(asyncDispatch(resultActions
                    .andExpect(request().asyncResult(anything()))
                    .andReturn()));
        } else {
            return resultActions;
        }
    }

    /**
     *  Scenario:
     *
     *  Given:
     *
     *  6 drivers with-in proximity of 9 kilometers:
     *
     *  1st at a distance of: 0 Kilometers with locationCoordinates=[65.12, 54.22]
     *  2nd at a distance of : 3.87 Kilometers with locationCoordinates=[65.09, 54.19]
     *  3rd at a distance of : 3.94 Kilometers with locationCoordinates=[65.07, 54.2]
     *  4th at a distance of : 4.63 Kilometers with locationCoordinates=[65.1, 54.18]
     *  5th at a distance of : 8.44 Kilometers with locationCoordinates=[65.07, 54.15]
     *  6th at a distance of : 9.03 Kilometers with locationCoordinates=[65.05, 54.15]
     *
     *  when:
     *  Customer is located at co-ordinates: latitude: 54.22, longitude: 65.12
     *  and Search of drivers with in the radius of 9 kilometers
     *
     *  Then:
     *  Assert that NearGeo (GeoSpatial Search) returns 5 drivers
     *  Each with a distance in the range of 0 to 9 kilometers of proximity
     *
     *  ** distance is available in the response Object.
     *
     */
    @Test
    public void test_find_All_Driver_GeoLogs_In_Proximity_Successfully() throws Exception {


        String userId_1 = "1211";
        String latitude_1 = "54.22";
        String longitude_1 = "65.12";
        String accuracy_1 = "100";

        insert_Test_Data(userId_1, latitude_1, longitude_1, accuracy_1);

        String userId_2 = "3999";
        String latitude_2 = "54.20";
        String longitude_2 = "65.07";
        String accuracy_2 = "100";

        insert_Test_Data(userId_2, latitude_2, longitude_2, accuracy_2);


        String userId_3 = "42000";
        String latitude_3 = "54.19";
        String longitude_3 = "65.09";
        String accuracy_3 = "100";

        insert_Test_Data(userId_3, latitude_3, longitude_3, accuracy_3);

        String userId_4 = "333";
        String latitude_4 = "54.18";
        String longitude_4 = "65.10";
        String accuracy_4 = "100";

        insert_Test_Data(userId_4, latitude_4, longitude_4, accuracy_4);

        String userId_5 = "8765";
        String latitude_5 = "54.15";
        String longitude_5 = "65.07";
        String accuracy_5 = "100";

        insert_Test_Data(userId_5, latitude_5, longitude_5, accuracy_5);

        String userId_6 = "8799";
        String latitude_6 = "54.15";
        String longitude_6 = "65.05";
        String accuracy_6 = "100";

        insert_Test_Data(userId_6, latitude_6, longitude_6, accuracy_6);


        String latitude_SearchParameter = "54.22";
        String longitude_SearchParameter = "65.12";
        String radius_SearchParameter = "9000";
        String limit_SearchParameter = "10";

        MvcResult mvcResult = mockMvc.perform(get("/drivers")
                .param("latitude", latitude_SearchParameter)
                .param("longitude", longitude_SearchParameter)
                .param("radius", radius_SearchParameter)
                .param("limit", limit_SearchParameter))
                .andExpect(request().asyncStarted())
                .andReturn();

        Object responseObject = mvcResult.getAsyncResult();

        assertNotNull(responseObject);
        assertTrue(responseObject instanceof ResponseEntity);

        ResponseEntity responseEntity = (ResponseEntity) responseObject;

        assertNotNull(responseEntity.getStatusCode());
        assertTrue(responseEntity.getStatusCode().equals(HttpStatus.OK));
        assertNotNull(responseEntity.getBody());
        assertTrue(responseEntity.getBody() instanceof List);

        List<DriverLocatorModel> driverLocatorModels = (List<DriverLocatorModel>) responseEntity.getBody();

        assertNotNull(driverLocatorModels);
        assertTrue(driverLocatorModels.size() == 5);

        String jsonString = objectMapper.writeValueAsString(driverLocatorModels);

        assertEquals("[{\"id\":1211,\"latitude\":54.22,\"longitude\":65.12,\"distance\":0}," +
                "{\"id\":42000,\"latitude\":54.19,\"longitude\":65.09,\"distance\":3868}," +
                "{\"id\":3999,\"latitude\":54.2,\"longitude\":65.07,\"distance\":3943}," +
                "{\"id\":333,\"latitude\":54.18,\"longitude\":65.1,\"distance\":4639}," +
                "{\"id\":8765,\"latitude\":54.15,\"longitude\":65.07,\"distance\":8445}]", jsonString);
    }

    /**
     *  Scenario:
     *
     *  Given:
     *
     *  6 drivers with-in proximity of 10 kilometers:
     *
     *  1st at a distance of: 0 Kilometers with locationCoordinates=[65.12, 54.22]
     *  2nd at a distance of : 3.87 Kilometers with locationCoordinates=[65.09, 54.19]
     *  3rd at a distance of : 3.94 Kilometers with locationCoordinates=[65.07, 54.2]
     *  4th at a distance of : 4.63 Kilometers with locationCoordinates=[65.1, 54.18]
     *  5th at a distance of : 8.44 Kilometers with locationCoordinates=[65.07, 54.15]
     *  6th at a distance of : 9.03 Kilometers with locationCoordinates=[65.05, 54.15]
     *
     *  when:
     *  Customer is located at co-ordinates: latitude: 54.22, longitude: 65.12
     *  and Search of drivers with in the radius of 4 kilometers
     *
     *  Then:
     *  Assert that NearGeo (GeoSpatial Search) returns 3 drivers
     *  Each with a distance in the range of 0 to 4 kilometers of proximity
     *
     *  ** distance is available in the response Object.
     *
     */
    @Test
    public void should_find_All_Drivers_WithIn_Closer_Proximity_Successfully() throws Exception {


        String userId_1  = "1211";
        String latitude_1 = "54.22" ;
        String longitude_1 = "65.12" ;
        String accuracy_1 = "100";

        insert_Test_Data(userId_1,latitude_1,longitude_1,accuracy_1);

        String userId_2  = "3999";
        String latitude_2 = "54.20" ;
        String longitude_2 = "65.07" ;
        String accuracy_2 = "100";

        insert_Test_Data(userId_2,latitude_2,longitude_2,accuracy_2);


        String userId_3  = "42000";
        String latitude_3 = "54.19";
        String longitude_3 = "65.09";
        String accuracy_3 = "100";

        insert_Test_Data(userId_3,latitude_3,longitude_3,accuracy_3);

        String userId_4  = "333";
        String latitude_4 = "54.18";
        String longitude_4 = "65.10";
        String accuracy_4 = "100";

        insert_Test_Data(userId_4,latitude_4,longitude_4,accuracy_4);

        String userId_5  = "8765";
        String latitude_5 = "54.15";
        String longitude_5 = "65.07";
        String accuracy_5 = "100";

        insert_Test_Data(userId_5,latitude_5,longitude_5,accuracy_5);

        String userId_6  = "8799";
        String latitude_6 = "54.15";
        String longitude_6 = "65.05";
        String accuracy_6 = "100";

        insert_Test_Data(userId_6,latitude_6,longitude_6,accuracy_6);


        String latitude_SearchParameter = "54.22";
        String longitude_SearchParameter = "65.12";
        String radius_SearchParameter = "4000";
        String limit_SearchParameter = "10";

        MvcResult mvcResult = mockMvc.perform(get("/drivers")
                .param("latitude", latitude_SearchParameter)
                .param("longitude", longitude_SearchParameter)
                .param("radius", radius_SearchParameter)
                .param("limit", limit_SearchParameter))
                .andExpect(request().asyncStarted())
                .andReturn();

        Object responseObject = mvcResult.getAsyncResult();

        assertNotNull(responseObject);
        assertTrue(responseObject instanceof ResponseEntity);

        ResponseEntity responseEntity = (ResponseEntity) responseObject;

        assertNotNull(responseEntity.getStatusCode());
        assertTrue(responseEntity.getStatusCode().equals(HttpStatus.OK));
        assertNotNull(responseEntity.getBody());
        assertTrue(responseEntity.getBody() instanceof List);

        List<DriverLocatorModel> driverLocatorModels = (List<DriverLocatorModel>) responseEntity.getBody();

        assertNotNull(driverLocatorModels);
        assertTrue(driverLocatorModels.size() == 3);

        String jsonString = objectMapper.writeValueAsString(driverLocatorModels);

        assertEquals("[{\"id\":1211,\"latitude\":54.22,\"longitude\":65.12,\"distance\":0}," +
                "{\"id\":42000,\"latitude\":54.19,\"longitude\":65.09,\"distance\":3868}," +
                "{\"id\":3999,\"latitude\":54.2,\"longitude\":65.07,\"distance\":3943}]", jsonString);

    }



    /**
     *  Scenario:
     *
     *  Given:
     *
     *  6 drivers with-in proximity of 10 kilometers:
     *
     *  1st at a distance of: 0 Kilometers with locationCoordinates=[65.12, 54.22]
     *  2nd at a distance of : 3.87 Kilometers with locationCoordinates=[65.09, 54.19]
     *  3rd at a distance of : 3.94 Kilometers with locationCoordinates=[65.07, 54.2]
     *  4th at a distance of : 4.63 Kilometers with locationCoordinates=[65.1, 54.18]
     *  5th at a distance of : 8.44 Kilometers with locationCoordinates=[65.07, 54.15]
     *  6th at a distance of : 9.03 Kilometers with locationCoordinates=[65.05, 54.15]
     *
     *  when:
     *  Customer is located at co-ordinates: latitude: 54.22, longitude: 65.12
     *  and Search of drivers with in the radius of 100 meters
     *
     *  Then:
     *  Assert that NearGeo (GeoSpatial Search) returns 1 driver
     *  Each with a distance in the range of 0 to 100 meters of proximity
     *
     *  ** distance is available in the response Object.
     *
     */
    @Test
    public void should_Assert_Behavior_For_SuperCloser_Proximity() throws Exception {


        String userId_1  = "1211";
        String latitude_1 = "54.22" ;
        String longitude_1 = "65.12" ;
        String accuracy_1 = "100";

        insert_Test_Data(userId_1,latitude_1,longitude_1,accuracy_1);

        String userId_2  = "3999";
        String latitude_2 = "54.20" ;
        String longitude_2 = "65.07" ;
        String accuracy_2 = "100";

        insert_Test_Data(userId_2,latitude_2,longitude_2,accuracy_2);


        String userId_3  = "42000";
        String latitude_3 = "54.19";
        String longitude_3 = "65.09";
        String accuracy_3 = "100";

        insert_Test_Data(userId_3,latitude_3,longitude_3,accuracy_3);

        String userId_4  = "333";
        String latitude_4 = "54.18";
        String longitude_4 = "65.10";
        String accuracy_4 = "100";

        insert_Test_Data(userId_4,latitude_4,longitude_4,accuracy_4);

        String userId_5  = "8765";
        String latitude_5 = "54.15";
        String longitude_5 = "65.07";
        String accuracy_5 = "100";

        insert_Test_Data(userId_5,latitude_5,longitude_5,accuracy_5);

        String userId_6  = "8799";
        String latitude_6 = "54.15";
        String longitude_6 = "65.05";
        String accuracy_6 = "100";

        insert_Test_Data(userId_6,latitude_6,longitude_6,accuracy_6);


        String latitude_SearchParameter = "54.22";
        String longitude_SearchParameter = "65.12";
        String radius_SearchParameter = "100";
        String limit_SearchParameter = "10";

        MvcResult mvcResult = mockMvc.perform(get("/drivers")
                .param("latitude", latitude_SearchParameter)
                .param("longitude", longitude_SearchParameter)
                .param("radius", radius_SearchParameter)
                .param("limit", limit_SearchParameter))
                .andExpect(request().asyncStarted())
                .andReturn();

        Object responseObject = mvcResult.getAsyncResult();

        assertNotNull(responseObject);
        assertTrue(responseObject instanceof ResponseEntity);

        ResponseEntity responseEntity = (ResponseEntity) responseObject;

        assertNotNull(responseEntity.getStatusCode());
        assertTrue(responseEntity.getStatusCode().equals(HttpStatus.OK));
        assertNotNull(responseEntity.getBody());
        assertTrue(responseEntity.getBody() instanceof List);

        List<DriverLocatorModel> driverLocatorModels = (List<DriverLocatorModel>) responseEntity.getBody();

        assertNotNull(driverLocatorModels);
        assertTrue(driverLocatorModels.size() == 1);

        String jsonString = objectMapper.writeValueAsString(driverLocatorModels);

        assertEquals("[{\"id\":1211,\"latitude\":54.22,\"longitude\":65.12,\"distance\":0}]", jsonString);
    }

    @Test
    public void should_Assert_Behavior_For_No_AvailableDrivers_In_Proximity() throws Exception {


        String userId_1  = "1211";
        String latitude_1 = "54.22" ;
        String longitude_1 = "65.12" ;
        String accuracy_1 = "100";

        insert_Test_Data(userId_1,latitude_1,longitude_1,accuracy_1);

        String userId_2  = "3999";
        String latitude_2 = "54.20" ;
        String longitude_2 = "65.07" ;
        String accuracy_2 = "100";

        insert_Test_Data(userId_2,latitude_2,longitude_2,accuracy_2);


        String userId_3  = "42000";
        String latitude_3 = "54.19";
        String longitude_3 = "65.09";
        String accuracy_3 = "100";

        insert_Test_Data(userId_3,latitude_3,longitude_3,accuracy_3);

        String userId_4  = "333";
        String latitude_4 = "54.18";
        String longitude_4 = "65.10";
        String accuracy_4 = "100";

        insert_Test_Data(userId_4,latitude_4,longitude_4,accuracy_4);

        String userId_5  = "8765";
        String latitude_5 = "54.15";
        String longitude_5 = "65.07";
        String accuracy_5 = "100";

        insert_Test_Data(userId_5,latitude_5,longitude_5,accuracy_5);

        String userId_6  = "8799";
        String latitude_6 = "54.15";
        String longitude_6 = "65.05";
        String accuracy_6 = "100";

        insert_Test_Data(userId_6,latitude_6,longitude_6,accuracy_6);



        String latitude_SearchParameter = "74.22";
        String longitude_SearchParameter = "65.12";
        String radius_SearchParameter = "100";
        String limit_SearchParameter = "10";

        MvcResult mvcResult = mockMvc.perform(get("/drivers")
                .param("latitude", latitude_SearchParameter)
                .param("longitude", longitude_SearchParameter)
                .param("radius", radius_SearchParameter)
                .param("limit", limit_SearchParameter))
                .andExpect(request().asyncStarted())
                .andReturn();

        Object responseObject = mvcResult.getAsyncResult();

        assertNotNull(responseObject);
        assertTrue(responseObject instanceof ResponseEntity);

        ResponseEntity responseEntity = (ResponseEntity) responseObject;

        assertNotNull(responseEntity.getStatusCode());
        assertTrue(responseEntity.getStatusCode().equals(HttpStatus.OK));
        assertNotNull(responseEntity.getBody());
        assertTrue(responseEntity.getBody() instanceof List);

        List<DriverLocatorModel> driverLocatorModels = (List<DriverLocatorModel>) responseEntity.getBody();

        assertNotNull(driverLocatorModels);
        assertTrue(driverLocatorModels.size() == 0);

        String jsonString = objectMapper.writeValueAsString(driverLocatorModels);

        assertEquals("[]", jsonString);
    }

    @Test
    public void should_Assert_Behavior_For_Validation_Errors() throws Exception {


        String userId_1  = "1211";
        String latitude_1 = "54.22" ;
        String longitude_1 = "65.12" ;
        String accuracy_1 = "100";

        insert_Test_Data(userId_1,latitude_1,longitude_1,accuracy_1);

        String userId_2  = "3999";
        String latitude_2 = "54.20" ;
        String longitude_2 = "65.07" ;
        String accuracy_2 = "100";

        insert_Test_Data(userId_2,latitude_2,longitude_2,accuracy_2);


        String userId_3  = "42000";
        String latitude_3 = "54.19";
        String longitude_3 = "65.09";
        String accuracy_3 = "100";

        insert_Test_Data(userId_3,latitude_3,longitude_3,accuracy_3);

        String userId_4  = "333";
        String latitude_4 = "54.18";
        String longitude_4 = "65.10";
        String accuracy_4 = "100";

        insert_Test_Data(userId_4,latitude_4,longitude_4,accuracy_4);

        String userId_5  = "8765";
        String latitude_5 = "54.15";
        String longitude_5 = "65.07";
        String accuracy_5 = "100";

        insert_Test_Data(userId_5,latitude_5,longitude_5,accuracy_5);

        String userId_6  = "8799";
        String latitude_6 = "54.15";
        String longitude_6 = "65.05";
        String accuracy_6 = "100";

        insert_Test_Data(userId_6,latitude_6,longitude_6,accuracy_6);

        String latitude_SearchParameter = "174.22";
        String longitude_SearchParameter = "265.12";
        String radius_SearchParameter = "100000";
        String limit_SearchParameter = "10";

        MvcResult mvcResult = mockMvc.perform(get("/drivers")
                .param("latitude", latitude_SearchParameter)
                .param("longitude", longitude_SearchParameter)
                .param("radius", radius_SearchParameter)
                .param("limit", limit_SearchParameter))
                .andExpect(request().asyncStarted())
                .andReturn();

        Object responseObject = mvcResult.getAsyncResult();

        assertNotNull(responseObject);
        assertTrue(responseObject instanceof ResponseEntity);

        ResponseEntity responseEntity = (ResponseEntity) responseObject;

        assertNotNull(responseEntity.getStatusCode());
        assertTrue(responseEntity.getStatusCode().equals(HttpStatus.UNPROCESSABLE_ENTITY));
        assertNotNull(responseEntity.getBody());
        assertTrue(responseEntity.getBody() instanceof ErrorModel);

        ErrorModel errorModel = (ErrorModel) responseEntity.getBody();

        assertNotNull(errorModel);
        assertTrue(errorModel.getErrors().size() == 3);

        String jsonString = objectMapper.writeValueAsString(errorModel);

        assertEquals("{\"errors\":[\"Latitude should be between +/- 90\"," +
                "\"Longitude should be between +/- 180\"," +
                "\"Unacceptable radius value\"]}", jsonString);

    }


    private DriverGeo insert_Test_Data(String userId, String latitude, String longitude,String accuracy){

        DriverGeo driverGeo =
                prepareDriverGeoEntityFromParameters(Long.valueOf(userId), Double.valueOf(latitude), Double.valueOf(longitude), Double.valueOf(accuracy));

        return mongoTemplate.save(driverGeo).block();
    }

    private DriverGeo prepareDriverGeoEntityFromParameters(Long userId, Double latitude, Double longitude, Double accuracy) {
        Double[] locationCordinates = new Double[2];

        locationCordinates[0] = longitude;
        locationCordinates[1] = latitude;

        return DriverGeo.builder().userId(userId).latitude(latitude)
                .longitude(longitude).accuracy(accuracy).location(new GeoJsonPoint(longitude,latitude))
                .locationCoordinates(locationCordinates).build();
    }

}
