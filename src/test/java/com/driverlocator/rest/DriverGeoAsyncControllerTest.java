package com.driverlocator.rest;


import com.driverlocator.configuration.DriverLocatorIntegrationTestConfig;
import com.driverlocator.configuration.MongoTestConfig;
import com.driverlocator.model.DriverGeoModel;
import com.driverlocator.model.ErrorModel;
import com.driverlocator.repository.DriverGeoLogRepository;
import com.driverlocator.repository.DriverGeoRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import static org.junit.Assert.*;

/**
 * Test cases for the {@link DriverGeoAsyncController}.
 *
 * @author lakshmikanth
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes ={MongoTestConfig.class, DriverLocatorIntegrationTestConfig.class})
@DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
public class DriverGeoAsyncControllerTest {

    Logger log = LoggerFactory.getLogger(DriverGeoAsyncControllerTest.class);

    @Autowired
    DriverGeoAsyncController driverGeoAsyncController;

    @Autowired
    DriverGeoRepository driverGeoRepository;

    @Autowired
    DriverGeoLogRepository driverGeoLogRepository;

    @Autowired
    ReactiveMongoTemplate mongoTemplate;


    @Autowired
    ThreadPoolTaskExecutor asyncTestTaskExecutor;

    @Before
    public void setUp() {
        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }

    @After
    public void cleanup() {
        driverGeoRepository.deleteAll();
        driverGeoLogRepository.deleteAll();
    }


    @Test
    public void assert_Record_Driver_Geo_Location_Details_Successfully() throws Exception{

        //given

        Long userId = Long.valueOf(7612);
        Double latitude = Double.valueOf(43.21);
        Double longitude = Double.valueOf(54.32);
        Double accuracy = Double.valueOf(100);

        DriverGeoModel driverGeoModel =
                getDriverGeoModelFromParameters(userId,latitude,longitude,accuracy);

        //when
        Callable<ResponseEntity> responseEntityCallable =
                driverGeoAsyncController.recordDriverGeoLocation(userId,driverGeoModel);


        ResponseEntity responseEntity =  asyncTestTaskExecutor.submit(responseEntityCallable).get();

        //then
        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertEquals("{}",responseEntity.getBody());
        assertNotNull(responseEntity.getStatusCode());
        assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
    }


    @Test
    public void assert_Record_Driver_Geo_Location_Details_With_Invalid_Driver() throws Exception{

        //given

        Long userId = Long.valueOf(76121);
        Double latitude = Double.valueOf(43.21);
        Double longitude = Double.valueOf(54.32);
        Double accuracy = Double.valueOf(100);

        DriverGeoModel driverGeoModel =
                getDriverGeoModelFromParameters(userId,latitude,longitude,accuracy);


        //when
        Callable<ResponseEntity> responseEntityCallable =
                driverGeoAsyncController.recordDriverGeoLocation(userId,driverGeoModel);


        ResponseEntity responseEntity =  asyncTestTaskExecutor.submit(responseEntityCallable).get();

        //then
        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertEquals("{}",responseEntity.getBody());
        assertNotNull(responseEntity.getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND,responseEntity.getStatusCode());
    }

    @Test
    public void assert_Record_Driver_Geo_Location_Details_With_Invalid_Coordinates() throws Exception{

        //given

        Long userId = Long.valueOf(9121);
        Double latitude = Double.valueOf(91.21);
        Double longitude = Double.valueOf(190.32);
        Double accuracy = Double.valueOf(100);

        DriverGeoModel driverGeoModel =
                getDriverGeoModelFromParameters(userId,latitude,longitude,accuracy);


        //when
        Callable<ResponseEntity> responseEntityCallable =
                driverGeoAsyncController.recordDriverGeoLocation(userId,driverGeoModel);


        ResponseEntity responseEntity =  asyncTestTaskExecutor.submit(responseEntityCallable).get();

        //then
        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertTrue(responseEntity.getBody() instanceof ErrorModel);
        assertNotNull(responseEntity.getStatusCode());
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY,responseEntity.getStatusCode());

        //assert ResponseEntity-Body
        ErrorModel errorModel =
                (ErrorModel)responseEntity.getBody();

        assertNotNull(errorModel);
        assertNotNull(errorModel.getErrors());
        assertEquals(2,errorModel.getErrors().size());

        List<String> messages_Expected = Arrays.asList("Latitude should be between +/- 90","Longitude should be between +/- 180");

        assertEquals(messages_Expected,errorModel.getErrors());
    }



    public DriverGeoModel getDriverGeoModelFromParameters(Long userId,Double latitude,Double longitude,Double accuracy){
        return DriverGeoModel.builder().userId(userId).latitude(latitude).longitude(longitude).accuracy(accuracy).build();
    }





}
