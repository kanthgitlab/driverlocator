package com.driverlocator.rest;

import com.driverlocator.command.DriverLocationCaptureCommand;
import com.driverlocator.model.DriverGeoModel;
import com.driverlocator.service.DriverGeoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.Callable;

@RestController
public class DriverGeoAsyncController {

    Logger log = LoggerFactory.getLogger(DriverGeoAsyncController.class);

    DriverGeoService driverGeoService;

    private ThreadPoolTaskExecutor asyncTaskExecutor;

    @Autowired
    public DriverGeoAsyncController(DriverGeoService driverGeoService,ThreadPoolTaskExecutor asyncTaskExecutor) {
        this.driverGeoService = driverGeoService;
        this.asyncTaskExecutor = asyncTaskExecutor;
    }

    @RequestMapping(value = "/drivers/{id}/location" , method = RequestMethod.PUT)
    @ResponseBody
    public Callable<ResponseEntity> recordDriverGeoLocation(@PathVariable Long id,
                                                           @RequestBody DriverGeoModel driverGeoModel){

        log.info("recording location: {}",driverGeoModel);

        driverGeoModel.setUserId(id);

       return  new DriverLocationCaptureCommand(driverGeoModel,driverGeoService);
    }


}
