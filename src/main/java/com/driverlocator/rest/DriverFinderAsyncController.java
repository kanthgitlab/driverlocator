package com.driverlocator.rest;

import com.driverlocator.command.DriverFinderQueryCommand;
import com.driverlocator.model.DriverLocatorRequestModel;
import com.driverlocator.service.DriverFinderService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
public class DriverFinderAsyncController {

    Logger log = LoggerFactory.getLogger(DriverFinderAsyncController.class);

    private DriverFinderService driverFinderService;

    private ThreadPoolTaskExecutor asyncTaskExecutor;

    @Autowired
    public DriverFinderAsyncController(DriverFinderService driverFinderService, ThreadPoolTaskExecutor asyncTaskExecutor) {
        this.driverFinderService = driverFinderService;
        this.asyncTaskExecutor = asyncTaskExecutor;
    }

    @RequestMapping(value = "/drivers",method = RequestMethod.GET)
    public Callable<ResponseEntity> getDriversInProximity(
            @RequestParam String latitude,
            @RequestParam String longitude,
            @RequestParam(required = false) String radius,
            @RequestParam(required = false) String limit){


        Double latitudeParam = Double.valueOf(latitude);
        Double longitudeParam = Double.valueOf(longitude);
        Long radiusParam = StringUtils.isNotBlank(radius) ? Long.valueOf(radius) : Long.valueOf(500);
        Integer limitParam = StringUtils.isNotBlank(limit) ? Integer.valueOf(limit) : Integer.valueOf(10);

        log.info("about to search for driver geo-coordinates: latitude: {}, longitude: {}, radius: {},limit: {} "
                ,latitudeParam,longitudeParam,radiusParam,limitParam);

        DriverLocatorRequestModel driverLocatorRequestModel =
                DriverLocatorRequestModel.builder().latitude(latitudeParam).longitude(longitudeParam)
                        .radius(radiusParam).limit(limitParam).build();

        return new DriverFinderQueryCommand(driverLocatorRequestModel,driverFinderService);
    }

}
