package com.driverlocator.repository;

import com.driverlocator.entity.DriverGeoLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class DriverGeoLogRepositoryImpl implements DriverGeoLogRepository {

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;

    /**
     * @param userId
     * @return DriverGeo
     */
    @Override
    public Flux<DriverGeoLog> getDriverGeoLogsByUserId(Long userId) {

        Query query = query(where("userId").is(userId));

        return reactiveMongoTemplate.find(query,DriverGeoLog.class );
    }

    /**
     * @param driverGeoLog
     * @return Mono<DriverGeoLog>
     */
    @Override
    public DriverGeoLog save(DriverGeoLog driverGeoLog) {
        return reactiveMongoTemplate.save(driverGeoLog).block();
    }

    /**
     * delete all documents
     */
    @Override
    public void deleteAll() {
        //reactiveMongoTemplate.remove(DriverGeoLog.class);

        reactiveMongoTemplate.remove(new Query(), "DriverGeoLog").then(Mono.empty()).block();
    }
}
