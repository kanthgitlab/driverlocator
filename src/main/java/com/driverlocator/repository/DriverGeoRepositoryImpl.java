package com.driverlocator.repository;

import com.driverlocator.entity.DriverGeo;
import com.driverlocator.model.DriverLocatorQueryParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.geoNear;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class DriverGeoRepositoryImpl implements DriverGeoRepository {

    Logger log = LoggerFactory.getLogger(DriverGeoRepositoryImpl.class);

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;

    /**
     * @return Flux
     */
    @Override
    public Flux<DriverGeo> findAll() {
        return reactiveMongoTemplate.findAll(DriverGeo.class);
    }

    /**
     * @param userId
     * @return Flux
     */
    @Override
    public Flux<DriverGeo> findAllByUserId(Long userId) {

        Query query = query(where("userId").is(userId));

        return reactiveMongoTemplate.find(query,DriverGeo.class );
    }

    /**
     * @param userId
     * @return DriverGeo
     */
    @Override
    public DriverGeo checkDriverEntry(Long userId) {

        Query query = query(where("userId").is(userId));

        return reactiveMongoTemplate.findOne(query,DriverGeo.class ).block();
    }

    /**
     *
     * @param driverLocatorQueryParameters
     * @return List<DriverGeo>
     */
    @Override
    public List<DriverGeo> findNearestDriversWithInRadius(DriverLocatorQueryParameters driverLocatorQueryParameters){

        Double radiusInKilometers = Double.valueOf(driverLocatorQueryParameters.getRadius())/1000;

        final NearQuery query = NearQuery.near(new Point(driverLocatorQueryParameters.getLongitude(), driverLocatorQueryParameters.getLatitude()), Metrics.KILOMETERS)
                .num(driverLocatorQueryParameters.getLimit())
                .minDistance(0, Metrics.KILOMETERS)
                .maxDistance(radiusInKilometers,Metrics.KILOMETERS)
                .spherical(true);

        final Aggregation a = newAggregation(geoNear(query, "distance"));

        final Flux<DriverGeo> results = reactiveMongoTemplate.aggregate(a, DriverGeo.class, DriverGeo.class);

        List<DriverGeo> driverGeos = results.toStream().collect(Collectors.toList());

        return driverGeos;
   }

    @Override
    public Mono<DriverGeo> save(DriverGeo driverGeo_updateEvent) {
        return reactiveMongoTemplate.save(driverGeo_updateEvent);
    }

    /**
     * delete all documents
     */
    @Override
    public void deleteAll() {
       // reactiveMongoTemplate.remove(DriverGeo.class);

        reactiveMongoTemplate.remove(new Query(), "DriverGeo").then(Mono.empty()).block();
    }

}
