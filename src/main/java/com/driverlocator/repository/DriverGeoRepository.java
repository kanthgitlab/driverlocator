package com.driverlocator.repository;

import com.driverlocator.entity.DriverGeo;
import com.driverlocator.model.DriverLocatorQueryParameters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


public interface DriverGeoRepository  {


    /**
     *
     * @return Flux
     */
    public Flux<DriverGeo> findAll();

    /**
     * @param  userId
     * @return Flux
     */
    public Flux<DriverGeo> findAllByUserId(Long userId);


    /**
     *
     * @param userId
     * @return DriverGeo
     */
    public DriverGeo checkDriverEntry(Long userId) ;

    /**
     *
     * @param driverLocatorQueryParameters
     * @return List<DriverGeo>
     */
    List<DriverGeo> findNearestDriversWithInRadius(DriverLocatorQueryParameters driverLocatorQueryParameters);


    Mono<DriverGeo> save(DriverGeo driverGeo_updateEvent);

    /**
     * delete all documents
     */
    void deleteAll();
}

