package com.driverlocator.repository;

import com.driverlocator.entity.DriverGeoLog;
import reactor.core.publisher.Flux;

/**
 * Record Driver's GeoLog
 * Create a new DriverGeoLog record in DriverGeoLog Collection
 */
public interface DriverGeoLogRepository  {


    /**
     *
     * @param userId
     * @return DriverGeoLog
     */
    public Flux<DriverGeoLog> getDriverGeoLogsByUserId(Long userId) ;

    /**
     *
     * @param driverGeoLog
     * @return Mono<DriverGeoLog>
     */
    DriverGeoLog save(DriverGeoLog driverGeoLog);


    /**
     * delete all documents
     */
    void deleteAll();
}

