package com.driverlocator.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class DriverLocatorAsycConfigurer {


    @Value("${async.thread.core-pool}")
    private int corePoolSize;

    @Value("${async.thread.max-pool}")
    private int maxPoolSize;

    @Value("${async.queue.capacity}")
    private int queueCapacity;

    @Value("${async.thread.timeout}")
    private int threadTimeout;


    @Bean
    @Qualifier("asyncTaskExecutor")
    WebMvcConfigurer configurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
                ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
                threadPoolTaskExecutor.setCorePoolSize(corePoolSize);
                threadPoolTaskExecutor.setMaxPoolSize(maxPoolSize);
                threadPoolTaskExecutor.setQueueCapacity(queueCapacity);
                threadPoolTaskExecutor.setKeepAliveSeconds(threadTimeout);
                threadPoolTaskExecutor.setThreadNamePrefix("DriverLocatorAsyncProcessor-");
                threadPoolTaskExecutor.initialize();
                configurer.setTaskExecutor(threadPoolTaskExecutor);
            }
        };
    }
}
