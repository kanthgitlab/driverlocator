package com.driverlocator.configuration;

import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.async.client.MongoClientSettings;
import com.mongodb.connection.ClusterSettings;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoReactiveRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoReactiveAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;

import java.util.Arrays;

@Configuration
@ConfigurationProperties(prefix="mongo")
@EnableAutoConfiguration(exclude = { EmbeddedMongoAutoConfiguration.class ,
        MongoAutoConfiguration.class, MongoDataAutoConfiguration.class,
        MongoReactiveAutoConfiguration.class, MongoRepositoriesAutoConfiguration.class,
        MongoReactiveRepositoriesAutoConfiguration.class})
@ComponentScan(basePackages = "com.driverlocator.repository")
@ConditionalOnProperty(value="appMode", havingValue="production")
public class MongoConfig extends AbstractReactiveMongoConfiguration {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MongoConfig.class);

    @Value("${mongo.host}")
    private String host;
    @Value("${mongo.dbName}")
    private String dbName;
    @Value("${mongo.user}")
    private String user;
    @Value("${mongo.password}")
    private String password;


    /**
     * Return the Reactive Streams {@link MongoClient} instance to connect to. Annotate with {@link Bean} in case you want
     * to expose a {@link MongoClient} instance to the {@link ApplicationContext}.
     *
     * @return never {@literal null}.
     */
    @Override
    public MongoClient reactiveMongoClient() {

        ClusterSettings clusterSettings = ClusterSettings.builder()
                .hosts(Arrays.asList(new ServerAddress(host)))
                .build();

        MongoCredential mongoCredential = MongoCredential.createCredential(user, dbName, password.toCharArray());

        MongoClientSettings settings =
                MongoClientSettings.builder()
                        .credentialList(Arrays.asList(mongoCredential))
                .clusterSettings(clusterSettings).build();
        return MongoClients.create(settings);
        //return MongoClients.create(String.format("mongodb://%s", host));

    }


    @Bean
    public ReactiveMongoTemplate mongoTemplate() throws Exception {
        return new ReactiveMongoTemplate(reactiveMongoClient(), dbName);
    }


    @Bean
    public MongoCredential mongoCredentials() throws Exception {
        return MongoCredential.createCredential(user, dbName, password.toCharArray());
    }


    /**
     * mandatory method to be overridden for GRIDFS Template
     * @return
     */
    @Override
    protected String getDatabaseName() {
        return dbName;
    }

}
