package com.driverlocator.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class DriverLocatorModel {

    @JsonProperty("id")
    private Long userId;

    private Double latitude;

    private Double longitude;

    private Integer distance;

}
