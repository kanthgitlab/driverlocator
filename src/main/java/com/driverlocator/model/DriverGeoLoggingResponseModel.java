package com.driverlocator.model;

import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class DriverGeoLoggingResponseModel {

    private Boolean isValid;

    private List<String> message;
}
