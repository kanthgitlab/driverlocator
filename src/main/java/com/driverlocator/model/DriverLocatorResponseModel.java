package com.driverlocator.model;

import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class DriverLocatorResponseModel {

    private List<DriverLocatorModel> nearestDrivers;

    private Boolean isValid;

    private List<String> message;
}
