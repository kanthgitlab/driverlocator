package com.driverlocator.model;

import lombok.*;

@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class DriverLocatorRequestModel {

    private Double latitude;
    private Double longitude;
    private Long radius;
    private Integer limit;
}
