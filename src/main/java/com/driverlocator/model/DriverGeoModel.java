package com.driverlocator.model;

import lombok.*;

@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class DriverGeoModel {

    private Long userId;

    private Double longitude;

    private Double latitude;

    private Double accuracy;

    private Long radius;

}
