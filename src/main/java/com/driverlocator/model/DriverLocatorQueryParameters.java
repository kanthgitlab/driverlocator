package com.driverlocator.model;

import lombok.*;

@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class DriverLocatorQueryParameters {

    Long radius ;
    Integer limit;
    Double latitude;
    Double longitude;
}
