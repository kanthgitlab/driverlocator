package com.driverlocator.entity;

import lombok.*;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Data
@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "DriverGeoLog")
public class DriverGeoLog {

    @Id
    private String id;

    private Long userId;

    private Double longitude;

    private Double latitude;

    private Double accuracy;

    private Double[] locationCoordinates;

    private GeoJsonPoint location;

    private Long timeInMillisecondsEpoch;
}
