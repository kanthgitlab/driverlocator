package com.driverlocator.service;

import com.driverlocator.entity.DriverGeo;
import com.driverlocator.entity.DriverGeoLog;
import com.driverlocator.model.DriverGeoLoggingResponseModel;
import com.driverlocator.model.DriverGeoModel;
import com.driverlocator.repository.DriverGeoLogRepository;
import com.driverlocator.repository.DriverGeoRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class DriverGeoServiceImpl implements DriverGeoService {

    Logger log = LoggerFactory.getLogger(DriverGeoServiceImpl.class);


    private DriverGeoRepository driverGeoRepository;

    private DriverGeoLogRepository driverGeoLogRepository;

    private ModelMapper modelMapper;

    @Autowired
    public DriverGeoServiceImpl( DriverGeoRepository driverGeoRepository, DriverGeoLogRepository driverGeoLogRepository, ModelMapper modelMapper) {

        this.driverGeoRepository = driverGeoRepository;
        this.driverGeoLogRepository = driverGeoLogRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public DriverGeoModel insertDriverLocationLogEvent(DriverGeoModel driverGeoModel) {

        //check Drivers validity
       DriverGeo driverGeo_ExistingEnity =
               driverGeoRepository.checkDriverEntry(driverGeoModel.getUserId());

       //declare updateEvent
        DriverGeo driverGeo_UpdateEvent = null;

        //This will be an upsert operation for entity: DriverGeo
        if(driverGeo_ExistingEnity!=null){
            driverGeo_UpdateEvent =  modelMapper.map(driverGeoModel, DriverGeo.class);
            driverGeo_UpdateEvent.setId(driverGeo_ExistingEnity.getId());
            driverGeo_UpdateEvent.setUserId(driverGeoModel.getUserId());
        } else{
            driverGeo_UpdateEvent = modelMapper.map(driverGeoModel, DriverGeo.class);
            driverGeo_UpdateEvent.setId(UUID.randomUUID().toString());
        }

        //get driver's location details as GeoJson Objects
        Double[] coordinates = new Double[2];
        coordinates[0] = driverGeoModel.getLongitude();
        coordinates[1] = driverGeoModel.getLatitude();
        driverGeo_UpdateEvent.setLocationCoordinates(coordinates);

        GeoJsonPoint geoJsonPoint = new GeoJsonPoint(coordinates[0],coordinates[1]);
        driverGeo_UpdateEvent.setLocation(geoJsonPoint);

        //Update the Driver's Geo Coordinates
        Mono<DriverGeo> driverGeoMono = driverGeoRepository.save(driverGeo_UpdateEvent);

        //create DriverGeoLog Event (Audit)
        DriverGeoLog driverGeoLog = getDriverGeoLogFromDriveGeoEvent(driverGeo_UpdateEvent);
        driverGeoLogRepository.save(driverGeoLog);

        DriverGeo driverGeo_Saved = driverGeoMono.block();

        return modelMapper.map(driverGeo_Saved,DriverGeoModel.class );
    }

    public static DriverGeoLog getDriverGeoLogFromDriveGeoEvent(DriverGeo driverGeo_UpdateEvent) {

        return DriverGeoLog.builder().id(UUID.randomUUID().toString())
                .userId(driverGeo_UpdateEvent.getUserId())
                .latitude(driverGeo_UpdateEvent.getLatitude())
                .longitude(driverGeo_UpdateEvent.getLongitude())
                .locationCoordinates(driverGeo_UpdateEvent.getLocationCoordinates())
                .location(driverGeo_UpdateEvent.getLocation()).build();
    }

    @Override
    public DriverGeoModel findDriverByUserId(Long userId) {
        DriverGeo driverGeo = driverGeoRepository.checkDriverEntry(userId);

        return driverGeo!=null ? modelMapper.map(driverGeo,DriverGeoModel.class ) : null;
    }

    @Override
    public DriverGeoLoggingResponseModel validateGeoLogEvent(DriverGeoModel driverGeoModel) {

            Double latitude_Parameter = driverGeoModel.getLatitude();

            Double longitude_Parameter = driverGeoModel.getLongitude();

            Long driverId = driverGeoModel.getUserId();

            List<String> messages = new ArrayList<>();

            Boolean isValid = Boolean.TRUE;

            if(driverId < 1 || driverId > 50000){
                isValid = Boolean.FALSE;
            }

            if(90 < latitude_Parameter || latitude_Parameter < -90){
                messages.add("Latitude should be between +/- 90");
                isValid = Boolean.FALSE;
            }

            if(180 < longitude_Parameter || longitude_Parameter < -180){
                messages.add("Longitude should be between +/- 180");
                isValid = Boolean.FALSE;
            }

        return DriverGeoLoggingResponseModel.builder().isValid(isValid).message(messages).build();
    }
}



