package com.driverlocator.service;

import com.driverlocator.entity.DriverGeo;
import com.driverlocator.model.DriverLocatorModel;
import com.driverlocator.model.DriverLocatorQueryParameters;
import com.driverlocator.model.DriverLocatorRequestModel;
import com.driverlocator.model.DriverLocatorResponseModel;
import com.driverlocator.repository.DriverGeoRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DriverFinderServiceImpl implements DriverFinderService {

    Logger log = LoggerFactory.getLogger(DriverFinderServiceImpl.class);

    DriverGeoRepository driverGeoRepository;

    private ModelMapper modelMapper;

    @Autowired
    public DriverFinderServiceImpl( DriverGeoRepository driverGeoRepository, ModelMapper modelMapper) {
        this.driverGeoRepository = driverGeoRepository;
        this.modelMapper = modelMapper;
    }

    /**
     *
     * @param driverLocatorRequestModel
     * @return
     */
    @Override
    public DriverLocatorResponseModel findMyNearestGoJeks(DriverLocatorRequestModel driverLocatorRequestModel){

        List<String> messages = validateLocatorRequestParameters(driverLocatorRequestModel);

        Boolean isValid = !messages.isEmpty() ? Boolean.FALSE : Boolean.TRUE;

        List<DriverLocatorModel> driverLocatorModels = null;

        if(isValid) {

            Double latitudeParam = driverLocatorRequestModel.getLatitude();
            Double longitudeParam = driverLocatorRequestModel.getLongitude();
            Long radius = driverLocatorRequestModel.getRadius();
            Integer limit = driverLocatorRequestModel.getLimit();

            List<DriverGeo> driverGeosExtracted = driverGeoRepository.findNearestDriversWithInRadius
                    (DriverLocatorQueryParameters.builder().latitude(latitudeParam).longitude(longitudeParam).radius(radius).limit(limit).build());

            log.debug("driverGeos Extracted: {}, {}", driverGeosExtracted, driverGeosExtracted.size());

            //collect filtered entity's latitude and longitude & distance is computed
             driverLocatorModels =
                    driverGeosExtracted.stream().map((driverGeoArgument) ->
                    {
                        DriverLocatorModel driverLocatorModel = modelMapper.map(driverGeoArgument, DriverLocatorModel.class);

                        driverLocatorModel.setDistance(Double.valueOf(driverGeoArgument.getDistance() * 1000).intValue());

                        return driverLocatorModel;
                    }).collect(Collectors.toList());
        }

        return DriverLocatorResponseModel.builder().nearestDrivers(driverLocatorModels).isValid(isValid).message(messages).build();
    }

    /**
     *
     * @param driverLocatorRequestModel
     * @return
     */
    @Override
    public List<String> validateLocatorRequestParameters(DriverLocatorRequestModel driverLocatorRequestModel) {

        Double latitude_Parameter = driverLocatorRequestModel.getLatitude();

        Double longitude_Parameter = driverLocatorRequestModel.getLongitude();

        Long radius_Parameter = driverLocatorRequestModel.getRadius();

        List<String> messages = new ArrayList<>();

        if(90 < latitude_Parameter || latitude_Parameter < -90){
            messages.add("Latitude should be between +/- 90");
        }

        if(180 < longitude_Parameter || longitude_Parameter < -180){
            messages.add("Longitude should be between +/- 180");
        }

        if(radius_Parameter < 0 || radius_Parameter >10000){
            messages.add("Unacceptable radius value");
        }
        return messages;
    }



}
