package com.driverlocator.service;

import com.driverlocator.model.DriverGeoLoggingResponseModel;
import com.driverlocator.model.DriverGeoModel;

public interface DriverGeoService {

    DriverGeoModel insertDriverLocationLogEvent(DriverGeoModel driverGeoModel) ;

    DriverGeoModel findDriverByUserId(Long userId);

    DriverGeoLoggingResponseModel validateGeoLogEvent(DriverGeoModel driverGeoModel);
}
