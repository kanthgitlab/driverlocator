package com.driverlocator.service;

import com.driverlocator.model.DriverLocatorRequestModel;
import com.driverlocator.model.DriverLocatorResponseModel;

import java.util.List;

public interface DriverFinderService {

     /**
      *
      * @param driverLocatorRequestModel
      * @return DriverLocatorResponseModel
      */
     DriverLocatorResponseModel findMyNearestGoJeks(DriverLocatorRequestModel driverLocatorRequestModel);


     List<String> validateLocatorRequestParameters(DriverLocatorRequestModel driverLocatorRequestModel);
}
