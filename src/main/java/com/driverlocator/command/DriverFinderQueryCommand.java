package com.driverlocator.command;

import com.driverlocator.model.DriverLocatorRequestModel;
import com.driverlocator.model.DriverLocatorResponseModel;
import com.driverlocator.model.ErrorModel;
import com.driverlocator.service.DriverFinderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.concurrent.Callable;

public class DriverFinderQueryCommand implements  Callable<ResponseEntity> {

    Logger log = LoggerFactory.getLogger(DriverFinderQueryCommand.class);

    private final DriverLocatorRequestModel driverLocatorRequestModel;
    private final DriverFinderService driverFinderService;


    public DriverFinderQueryCommand(DriverLocatorRequestModel driverLocatorRequestModel,
                                    DriverFinderService driverFinderService) {
        if (driverLocatorRequestModel == null) {
            throw new IllegalStateException("driverLocatorRequestModel is not set.");
        }

        if (driverFinderService == null) {
            throw new IllegalStateException("driverFinderService is not set.");
        }


        this.driverLocatorRequestModel = driverLocatorRequestModel;
        this.driverFinderService = driverFinderService;
    }

    @Override
    public ResponseEntity call() {

        DriverLocatorResponseModel driverLocatorResponseModel =
                driverFinderService.findMyNearestGoJeks(driverLocatorRequestModel);

        log.info("found {} drivers within geo-coordinates: latitude: {}, longitude: {}, radius: {},limit: {} "
                ,driverLocatorResponseModel,driverLocatorRequestModel.getLatitude(),
                driverLocatorRequestModel.getLongitude(),driverLocatorRequestModel.getRadius(),driverLocatorRequestModel.getLimit());

        HttpStatus responseStatus = null;

        List<String> messages = null;

        if(!driverLocatorResponseModel.getIsValid()){
            messages = driverLocatorResponseModel.getMessage();
            responseStatus = HttpStatus.UNPROCESSABLE_ENTITY;
        }else{
            responseStatus = HttpStatus.OK;
        }

        return !driverLocatorResponseModel.getIsValid() ?
                new ResponseEntity(ErrorModel.builder().errors(messages).build(),responseStatus)
                : ResponseEntity.ok(driverLocatorResponseModel.getNearestDrivers());
    }





}
