package com.driverlocator.command;

import com.driverlocator.model.DriverGeoLoggingResponseModel;
import com.driverlocator.model.DriverGeoModel;
import com.driverlocator.model.ErrorModel;
import com.driverlocator.service.DriverGeoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.concurrent.Callable;

public class DriverLocationCaptureCommand implements  Callable<ResponseEntity> {

    Logger log = LoggerFactory.getLogger(DriverLocationCaptureCommand.class);


    private final DriverGeoService driverGeoService;
    private final DriverGeoModel driverGeoModel;

    public DriverLocationCaptureCommand(DriverGeoModel driverGeoModel,
                                        DriverGeoService driverGeoService) {
        if (driverGeoModel == null) {
            throw new IllegalStateException("driverGeoModel is not set.");
        }

        if (driverGeoService == null) {
            throw new IllegalStateException("driverGeoService is not set.");
        }

        this.driverGeoService = driverGeoService;
        this.driverGeoModel = driverGeoModel;
    }

    @Override
    public ResponseEntity call() {


        log.info("recording location: {}",driverGeoModel);

        HttpStatus responseStatus = null;

        List<String> messages = null;

        DriverGeoLoggingResponseModel driverGeoLoggingResponseModel = null;

        Long id = driverGeoModel.getUserId();

        if(id < 1 || id > 50000){
            responseStatus = HttpStatus.NOT_FOUND;
        } else {

            driverGeoLoggingResponseModel =
                    driverGeoService.validateGeoLogEvent(driverGeoModel);

            if (!driverGeoLoggingResponseModel.getIsValid()) {
                messages = driverGeoLoggingResponseModel.getMessage();
                responseStatus = HttpStatus.UNPROCESSABLE_ENTITY;
            } else {
                driverGeoService.insertDriverLocationLogEvent(driverGeoModel);
                responseStatus = HttpStatus.OK;
            }
        }

        log.info("responseModel: {}",driverGeoLoggingResponseModel);

        return
                driverGeoLoggingResponseModel!=null ?
                        !driverGeoLoggingResponseModel.getIsValid() ?
                                new ResponseEntity(ErrorModel.builder().errors(messages).build(),responseStatus)
                                : ResponseEntity.ok("{}") :new ResponseEntity("{}",responseStatus);
    }





}
