# Driver Locator API

- API to locate nearest drivers
- API for drivers to record their current location



# Tech Stack

## Development:

  - Java-8
  - Spring-boot
  - Spring-reactive
  - Reactive-mongodb
  - Mongodb database

## Testing:

   - Junit
   - Mockito
   - Hamcrest Matchers
   - flapdoodle embedded-mongodb
   - Mockmvc tests
   - Integeration tests (spring-boot-test)

## Build and Deployment:

   -  Docker
   -  Spring-boot to run as embedded application in docker-container


# Design approach:

  - Async Web request handling using spring-boot webAsyncConfigurer and ThreadPoolExecutor
  - Callable Tasks for Command and Query endpoints
  - Reactive Style of programming and Reactive-Mongo library

# Geo-Spatial Search:

  - Drivers in proximity are identified using GeoSpatial search support provided by Mongodb & spring-data
  - NearBy Query is used for this implementation
  -Can be improved by running it on elasticSearch for faster and easier implementation


# Improvements Required:

 - Distributed processing via AKKA Actor based programming and design

   - Use AKKA support for Java to distribute the requests

 - Increase Mongodb clusters

 - Implement API Gateway (With load Balancer), Eureka Service Discovery and Hystrix Circuit Breaker (Netfix OSS)

 - Continuous deployment model to cloud

 - Implement event-Driven MicroServices and separate DriverGeo Location Logger and NearByDriversQuery as isolated micro-services

 - Remove Database coupling (no common database for command and query)


# API End-points

## Driver-Geo-Log API:

  -  Drivers should be able to send their current location every 60 seconds.

  - They’ll call following API to update their location

    ```
    http://<host>:<port>/drivers/:id/location
    ```

  - host and port mentioned may change
    - please refer the documentation below:  to run docker container
    - to identify the host and port to be used for API endpoint


  - Request: PUT /drivers/{id}/location
    ```js
        {
            "latitude": 12.97161923,
            "longitude": 77.59463452,
            "accuracy": 0.7
        }
     ```

  - Response:

    - 200 OK​ — on successful update, Body: {}
    - 404 Not Found​ — if the driver ID is invalid (valid driver ids - 1 to 50000) Body: {}
    - 422 Unprocessable Entity​ — with appropriate message. For example:
        ```
         {"errors":["Latitude should be between +/- 90"]}
        ```

## Find Drivers - API:

  -  Customer applications will use following API to find drivers around a given location

  - They’ll call following API to update their location

    ```
    http://<host>:<port>/drivers/
    ```

  - host and port mentioned may change

    - please refer the documentation below:  to run docker container
    - to identify the host and port to be used for API endpoint


  - Request: GET /drivers

     - Parameters:

        - "latitude"​ — mandatory
        - "longitude"​ — mandatory
        - "radius"​ — optional defaults to 500 meters "limit"​ — optional defaults to 10

 - Response:

    - 200 OK ​— on successful get, Body:
        ```
        [
          {id: 42, latitude: 12.97161923, longitude: 77.59463452, distance: 123},
          {id: 84, latitude: 12.97161923, longitude: 77.59463452, distance: 123}
         ]
         ```

    - 422 Unprocessable Entity ​— with appropriate message. For example:
    ```
      {"errors": ["Latitude should be between +/- 90"]}
    ```

    - Distance in the response is a straight line distance between driver's location and location in the query


# Docker build and Run details:

  1. Download and extract the ZIP file from google-drive
  2. Navigate to the extracted directory of the deliverables
  3. Extract should contain src, pom.xml and associated docker files: docker-compose.yml, Dockerfile-build.yml
  4. Ensure docker is installed and up, running in your machine

  5. If docker is installed and running, please do additional checks to double-confirm the status of docker on your machine/server

    - $ docker status

  6. After you ensure that docker is running, stay in the directory where you have extracted the code.

  7. please run the command below:
      ```
      ./DriverLocator_Bootstrap.sh
      ```
     - This is a single step initiation of entire setup, which will eventually do clean,compile,test,package,deploy and start application
     - Includes dependant mongodb download and startup as well.

    - If you want to initiate via docker command, run the step:

        ```
        docker-compose up --build
        ```

  - Command should create a container and download mongodb version 4.0.4 and run mongodb on port 27017 with admin as default user

  - Followed by docker build step which will:

    - Copy the code in to container
    - do mvn package step
    - after Successful execution of compilation, tests and packaging, the binary gets copied as app.jar in to destination folder
    - Application is a spring-boot instance, hence it will start on port 8081 as mentioned in configuration


# Identify host and port to Consume API endpoints (on docker)

- Application runs in docker container
- port 8081 is exposed port in docker-compose.yml
- To identify the host and port, please run command:

  ``` js
  docker port <container-name>
  ```

  - to identify container name, please run command:

  ``` js
  docker ps
  ```

   - docker ps should display containers which are UP

   ```
   lakshmikanth-MacBook-Pro:mongo lakshmikanth$ docker ps
   CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                               NAMES
   da50b727ac43        driverlocator       "/usr/local/bin/mvn-…"   20 minutes ago      Up 8 minutes        8080/tcp, 0.0.0.0:32779->8081/tcp   driverlocator
   98f9dbfb39fd        mongo:4.0.4         "docker-entrypoint.s…"   About an hour ago   Up 8 minutes        0.0.0.0:27017->27017/tcp            driverlocator_mongo-container_1

   lakshmikanth-MacBook-Pro:mongo lakshmikanth$ docker port da50b727ac43
   8081/tcp -> 0.0.0.0:32779

   ```


   # Take-away from the command execution:

      - whether you want to consume via postman or other REST-API client, use:

        http://0.0.0.0:32779/<endpoint>

# Docker Statistic details:

  -  process to monitor the statistics of the containers (memory,cpu)

  - execute command:

    ```js
    docker stats
    ```

    output should be:

    ```
    CONTAINER ID        NAME                              CPU %               MEM USAGE / LIMIT     MEM %               NET I/O             BLOCK I/O           PIDS
    da50b727ac43        driverlocator                     0.72%               390.8MiB / 1.952GiB   19.55%              90.4kB / 60.3kB     451kB / 0B          39
    98f9dbfb39fd        driverlocator_mongo-container_1   0.45%               41.38MiB / 1.952GiB   2.07%               58.3kB / 85kB       332kB / 4.85MB      28
    ```



# Application startup check:

  - docker-compose command will take a while to download all maven depenendencies from maven central repository
  - mongodb should be downloaded and started on port 27017 (default port)
  - package (clean,compile,test,package) phases of mave
  - application will be up and running (with connection to mongodb running in other container)

  - entire log extract is pasted below:

    ```
      lakshmikanth-MacBook-Pro:DriverLocator lakshmikanth$ docker-compose up
      Starting driverlocator_mongo-container_1 ... done
      Starting driverlocator                   ... done
      Attaching to driverlocator_mongo-container_1, driverlocator
      mongo-container_1  | 2018-11-29T13:49:41.208+0000 I CONTROL  [main] Automatically disabling TLS 1.0, to force-enable TLS 1.0 specify --sslDisabledProtocols 'none'
      mongo-container_1  | 2018-11-29T13:49:41.233+0000 I CONTROL  [initandlisten] MongoDB starting : pid=1 port=27017 dbpath=/data/db 64-bit host=98f9dbfb39fd
      mongo-container_1  | 2018-11-29T13:49:41.236+0000 I CONTROL  [initandlisten] db version v4.0.4
      mongo-container_1  | 2018-11-29T13:49:41.236+0000 I CONTROL  [initandlisten] git version: f288a3bdf201007f3693c58e140056adf8b04839
      mongo-container_1  | 2018-11-29T13:49:41.236+0000 I CONTROL  [initandlisten] OpenSSL version: OpenSSL 1.0.2g  1 Mar 2016
      mongo-container_1  | 2018-11-29T13:49:41.236+0000 I CONTROL  [initandlisten] allocator: tcmalloc
      mongo-container_1  | 2018-11-29T13:49:41.236+0000 I CONTROL  [initandlisten] modules: none
      mongo-container_1  | 2018-11-29T13:49:41.236+0000 I CONTROL  [initandlisten] build environment:
      mongo-container_1  | 2018-11-29T13:49:41.236+0000 I CONTROL  [initandlisten]     distmod: ubuntu1604
      mongo-container_1  | 2018-11-29T13:49:41.236+0000 I CONTROL  [initandlisten]     distarch: x86_64
      mongo-container_1  | 2018-11-29T13:49:41.236+0000 I CONTROL  [initandlisten]     target_arch: x86_64
      mongo-container_1  | 2018-11-29T13:49:41.236+0000 I CONTROL  [initandlisten] options: { net: { bindIpAll: true }, security: { authorization: "enabled" } }
      mongo-container_1  | 2018-11-29T13:49:41.237+0000 W STORAGE  [initandlisten] Detected unclean shutdown - /data/db/mongod.lock is not empty.
      mongo-container_1  | 2018-11-29T13:49:41.237+0000 I STORAGE  [initandlisten] Detected data files in /data/db created by the 'wiredTiger' storage engine, so setting the active storage engine to 'wiredTiger'.
      mongo-container_1  | 2018-11-29T13:49:41.237+0000 W STORAGE  [initandlisten] Recovering data from the last clean checkpoint.
      mongo-container_1  | 2018-11-29T13:49:41.237+0000 I STORAGE  [initandlisten]
      mongo-container_1  | 2018-11-29T13:49:41.237+0000 I STORAGE  [initandlisten] ** WARNING: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine
      mongo-container_1  | 2018-11-29T13:49:41.237+0000 I STORAGE  [initandlisten] **          See http://dochub.mongodb.org/core/prodnotes-filesystem
      mongo-container_1  | 2018-11-29T13:49:41.237+0000 I STORAGE  [initandlisten] wiredtiger_open config: create,cache_size=487M,session_max=20000,eviction=(threads_min=4,threads_max=4),config_base=false,statistics=(fast),log=(enabled=true,archive=true,path=journal,compressor=snappy),file_manager=(close_idle_time=100000),statistics_log=(wait=0),verbose=(recovery_progress),
      mongo-container_1  | 2018-11-29T13:49:42.458+0000 I STORAGE  [initandlisten] WiredTiger message [1543499382:458178][1:0x7f2ab43f8a40], txn-recover: Main recovery loop: starting at 10/5888 to 11/256
      mongo-container_1  | 2018-11-29T13:49:42.458+0000 I STORAGE  [initandlisten] WiredTiger message [1543499382:458793][1:0x7f2ab43f8a40], txn-recover: Recovering log 10 through 11
      mongo-container_1  | 2018-11-29T13:49:42.677+0000 I STORAGE  [initandlisten] WiredTiger message [1543499382:677225][1:0x7f2ab43f8a40], txn-recover: Recovering log 11 through 11
      mongo-container_1  | 2018-11-29T13:49:42.784+0000 I STORAGE  [initandlisten] WiredTiger message [1543499382:784321][1:0x7f2ab43f8a40], txn-recover: Set global recovery timestamp: 0
      mongo-container_1  | 2018-11-29T13:49:42.851+0000 I RECOVERY [initandlisten] WiredTiger recoveryTimestamp. Ts: Timestamp(0, 0)
      mongo-container_1  | 2018-11-29T13:49:43.003+0000 I FTDC     [initandlisten] Initializing full-time diagnostic data capture with directory '/data/db/diagnostic.data'
      mongo-container_1  | 2018-11-29T13:49:43.010+0000 I NETWORK  [initandlisten] waiting for connections on port 27017
      mongo-container_1  | 2018-11-29T13:49:44.018+0000 I FTDC     [ftdc] Unclean full-time diagnostic data capture shutdown detected, found interim file, some metrics may have been lost. OK
      driverlocator      |
      driverlocator      |   .   ____          _            __ _ _
      driverlocator      |  /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
      driverlocator      | ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
      driverlocator      |  \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
      driverlocator      |   '  |____| .__|_| |_|_| |_\__, | / / / /
      driverlocator      |  =========|_|==============|___/=/_/_/_/
      driverlocator      |  :: Spring Boot ::        (v2.0.5.RELEASE)
      driverlocator      |
      driverlocator      | 2018-11-29 13:49:45.904  INFO 1 --- [           main] c.d.DriverLocatorApplication             : Starting DriverLocatorApplication v1.0.0-SNAPSHOT on da50b727ac43 with PID 1 (/usr/src/java-app/app.jar started by root in /usr/src/java-app)
      driverlocator      | 2018-11-29 13:49:45.936  INFO 1 --- [           main] c.d.DriverLocatorApplication             : No active profile set, falling back to default profiles: default
      driverlocator      | 2018-11-29 13:49:46.275  INFO 1 --- [           main] ConfigServletWebServerApplicationContext : Refreshing org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext@4f4a7090: startup date [Thu Nov 29 13:49:46 UTC 2018]; root of context hierarchy
      driverlocator      | 2018-11-29 13:49:49.969  INFO 1 --- [           main] o.s.b.f.s.DefaultListableBeanFactory     : Overriding bean definition for bean 'requestMappingHandlerAdapter' with a different definition: replacing [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=org.springframework.web.reactive.config.DelegatingWebFluxConfiguration; factoryMethodName=requestMappingHandlerAdapter; initMethodName=null; destroyMethodName=(inferred); defined in org.springframework.web.reactive.config.DelegatingWebFluxConfiguration] with [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration$EnableWebMvcConfiguration; factoryMethodName=requestMappingHandlerAdapter; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [org/springframework/boot/autoconfigure/web/servlet/WebMvcAutoConfiguration$EnableWebMvcConfiguration.class]]
      driverlocator      | 2018-11-29 13:49:49.973  INFO 1 --- [           main] o.s.b.f.s.DefaultListableBeanFactory     : Overriding bean definition for bean 'requestMappingHandlerMapping' with a different definition: replacing [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=org.springframework.web.reactive.config.DelegatingWebFluxConfiguration; factoryMethodName=requestMappingHandlerMapping; initMethodName=null; destroyMethodName=(inferred); defined in org.springframework.web.reactive.config.DelegatingWebFluxConfiguration] with [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=true; factoryBeanName=org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration$EnableWebMvcConfiguration; factoryMethodName=requestMappingHandlerMapping; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [org/springframework/boot/autoconfigure/web/servlet/WebMvcAutoConfiguration$EnableWebMvcConfiguration.class]]
      driverlocator      | 2018-11-29 13:49:49.982  INFO 1 --- [           main] o.s.b.f.s.DefaultListableBeanFactory     : Overriding bean definition for bean 'resourceHandlerMapping' with a different definition: replacing [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=org.springframework.web.reactive.config.DelegatingWebFluxConfiguration; factoryMethodName=resourceHandlerMapping; initMethodName=null; destroyMethodName=(inferred); defined in org.springframework.web.reactive.config.DelegatingWebFluxConfiguration] with [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration$EnableWebMvcConfiguration; factoryMethodName=resourceHandlerMapping; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [org/springframework/boot/autoconfigure/web/servlet/WebMvcAutoConfiguration$EnableWebMvcConfiguration.class]]
      driverlocator      | 2018-11-29 13:49:50.217  INFO 1 --- [           main] o.s.b.f.s.DefaultListableBeanFactory     : Overriding bean definition for bean 'mongoTemplate' with a different definition: replacing [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=mongoConfig; factoryMethodName=mongoTemplate; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [com/driverlocator/configuration/MongoConfig.class]] with [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration; factoryMethodName=mongoTemplate; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [org/springframework/boot/autoconfigure/data/mongo/MongoDataAutoConfiguration.class]]
      driverlocator      | 2018-11-29 13:49:50.230  INFO 1 --- [           main] o.s.b.f.s.DefaultListableBeanFactory     : Overriding bean definition for bean 'reactiveMongoTemplate' with a different definition: replacing [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=mongoConfig; factoryMethodName=reactiveMongoTemplate; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [com/driverlocator/configuration/MongoConfig.class]] with [Root bean: class [null]; scope=; abstract=false; lazyInit=false; autowireMode=3; dependencyCheck=0; autowireCandidate=true; primary=false; factoryBeanName=org.springframework.boot.autoconfigure.data.mongo.MongoReactiveDataAutoConfiguration; factoryMethodName=reactiveMongoTemplate; initMethodName=null; destroyMethodName=(inferred); defined in class path resource [org/springframework/boot/autoconfigure/data/mongo/MongoReactiveDataAutoConfiguration.class]]
      driverlocator      | 2018-11-29 13:49:50.769  INFO 1 --- [           main] .s.d.r.c.RepositoryConfigurationDelegate : Multiple Spring Data modules found, entering strict repository configuration mode!
      driverlocator      | 2018-11-29 13:49:51.201  INFO 1 --- [           main] .s.d.r.c.RepositoryConfigurationDelegate : Multiple Spring Data modules found, entering strict repository configuration mode!
      driverlocator      | 2018-11-29 13:49:51.316  INFO 1 --- [           main] o.s.i.config.IntegrationRegistrar        : No bean named 'integrationHeaderChannelRegistry' has been explicitly defined. Therefore, a default DefaultHeaderChannelRegistry will be created.
      driverlocator      | 2018-11-29 13:49:52.315  INFO 1 --- [           main] faultConfiguringBeanFactoryPostProcessor : No bean named 'errorChannel' has been explicitly defined. Therefore, a default PublishSubscribeChannel will be created.
      driverlocator      | 2018-11-29 13:49:52.339  INFO 1 --- [           main] faultConfiguringBeanFactoryPostProcessor : No bean named 'taskScheduler' has been explicitly defined. Therefore, a default ThreadPoolTaskScheduler will be created.
      driverlocator      | 2018-11-29 13:49:52.598  INFO 1 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'modelMapperBean' of type [com.driverlocator.configuration.ModelMapperBean$$EnhancerBySpringCGLIB$$a61b51f3] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
      driverlocator      | 2018-11-29 13:49:53.057  INFO 1 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'org.springframework.integration.config.IntegrationManagementConfiguration' of type [org.springframework.integration.config.IntegrationManagementConfiguration$$EnhancerBySpringCGLIB$$b0f34b08] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
      driverlocator      | 2018-11-29 13:49:55.005  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8081 (http)
      driverlocator      | 2018-11-29 13:49:55.160  INFO 1 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
      driverlocator      | 2018-11-29 13:49:55.161  INFO 1 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet Engine: Apache Tomcat/8.5.34
      driverlocator      | 2018-11-29 13:49:55.215  INFO 1 --- [ost-startStop-1] o.a.catalina.core.AprLifecycleListener   : The APR based Apache Tomcat Native library which allows optimal performance in production environments was not found on the java.library.path: [/usr/java/packages/lib/amd64:/usr/lib/x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/lib/jni:/lib:/usr/lib]
      driverlocator      | 2018-11-29 13:49:55.542  INFO 1 --- [ost-startStop-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
      driverlocator      | 2018-11-29 13:49:55.542  INFO 1 --- [ost-startStop-1] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 9280 ms
      driverlocator      | 2018-11-29 13:49:57.035  INFO 1 --- [ost-startStop-1] org.mongodb.driver.cluster               : Cluster created with settings {hosts=[mongo-container:27017], mode=MULTIPLE, requiredClusterType=UNKNOWN, serverSelectionTimeout='30000 ms', maxWaitQueueSize=500}
      driverlocator      | 2018-11-29 13:49:57.036  INFO 1 --- [ost-startStop-1] org.mongodb.driver.cluster               : Adding discovered server mongo-container:27017 to client view of cluster
      mongo-container_1  | 2018-11-29T13:49:57.361+0000 I NETWORK  [listener] connection accepted from 172.20.0.3:51324 #1 (1 connection now open)
      mongo-container_1  | 2018-11-29T13:49:57.440+0000 I NETWORK  [conn1] received client metadata from 172.20.0.3:51324 conn1: { driver: { name: "mongo-java-driver|mongo-java-driver-reactivestreams", version: "unknown|unknown" }, os: { type: "Linux", name: "Linux", architecture: "amd64", version: "4.9.93-linuxkit-aufs" }, platform: "Java/Oracle Corporation/1.8.0_181-8u181-b13-2~deb9u1-b13" }
      mongo-container_1  | 2018-11-29T13:49:57.984+0000 I ACCESS   [conn1] Successfully authenticated as principal admin on admin
      driverlocator      | 2018-11-29 13:49:57.989  INFO 1 --- [container:27017] org.mongodb.driver.connection            : Opened connection [connectionId{localValue:1, serverValue:1}] to mongo-container:27017
      driverlocator      | 2018-11-29 13:49:58.016  INFO 1 --- [container:27017] org.mongodb.driver.cluster               : Monitor thread successfully connected to server with description ServerDescription{address=mongo-container:27017, type=STANDALONE, state=CONNECTED, ok=true, version=ServerVersion{versionList=[4, 0, 4]}, minWireVersion=0, maxWireVersion=7, maxDocumentSize=16777216, logicalSessionTimeoutMinutes=30, roundTripTimeNanos=22417000}
      driverlocator      | 2018-11-29 13:49:58.041  INFO 1 --- [container:27017] org.mongodb.driver.cluster               : Discovered cluster type of STANDALONE
      driverlocator      | 2018-11-29 13:49:58.911  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'characterEncodingFilter' to: [/*]
      driverlocator      | 2018-11-29 13:49:58.914  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'webMvcMetricsFilter' to: [/*]
      driverlocator      | 2018-11-29 13:49:58.914  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'hiddenHttpMethodFilter' to: [/*]
      driverlocator      | 2018-11-29 13:49:58.915  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'httpPutFormContentFilter' to: [/*]
      driverlocator      | 2018-11-29 13:49:58.916  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'requestContextFilter' to: [/*]
      driverlocator      | 2018-11-29 13:49:58.918  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'httpTraceFilter' to: [/*]
      driverlocator      | 2018-11-29 13:49:58.919  INFO 1 --- [ost-startStop-1] o.s.b.w.servlet.ServletRegistrationBean  : Servlet dispatcherServlet mapped to [/]
      driverlocator      | 2018-11-29 13:50:00.965  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/drivers],methods=[GET]}" onto public org.springframework.http.ResponseEntity com.driverlocator.rest.DriverFinderController.getDriverGeoLocationLogs(java.lang.String,java.lang.String,java.lang.String,java.lang.String)
      driverlocator      | 2018-11-29 13:50:00.968  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/drivers/{id}/location],methods=[PUT]}" onto public org.springframework.http.ResponseEntity com.driverlocator.rest.DriverGeoController.recordDriverGeoLocation(java.lang.Long,com.driverlocator.model.DriverGeoModel)
      driverlocator      | 2018-11-29 13:50:00.974  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/swagger-resources/configuration/ui]}" onto public org.springframework.http.ResponseEntity<springfox.documentation.swagger.web.UiConfiguration> springfox.documentation.swagger.web.ApiResourceController.uiConfiguration()
      driverlocator      | 2018-11-29 13:50:00.977  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/swagger-resources/configuration/security]}" onto public org.springframework.http.ResponseEntity<springfox.documentation.swagger.web.SecurityConfiguration> springfox.documentation.swagger.web.ApiResourceController.securityConfiguration()
      driverlocator      | 2018-11-29 13:50:00.981  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/swagger-resources]}" onto public org.springframework.http.ResponseEntity<java.util.List<springfox.documentation.swagger.web.SwaggerResource>> springfox.documentation.swagger.web.ApiResourceController.swaggerResources()
      driverlocator      | 2018-11-29 13:50:00.987  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error],produces=[text/html]}" onto public org.springframework.web.servlet.ModelAndView org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.errorHtml(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)
      driverlocator      | 2018-11-29 13:50:00.988  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error]}" onto public org.springframework.http.ResponseEntity<java.util.Map<java.lang.String, java.lang.Object>> org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController.error(javax.servlet.http.HttpServletRequest)
      driverlocator      | 2018-11-29 13:50:01.219  INFO 1 --- [           main] o.s.b.a.e.web.EndpointLinksResolver      : Exposing 2 endpoint(s) beneath base path '/actuator'
      driverlocator      | 2018-11-29 13:50:01.262  INFO 1 --- [           main] s.b.a.e.w.s.WebMvcEndpointHandlerMapping : Mapped "{[/actuator/health],methods=[GET],produces=[application/vnd.spring-boot.actuator.v2+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.web.servlet.AbstractWebMvcEndpointHandlerMapping$OperationHandler.handle(javax.servlet.http.HttpServletRequest,java.util.Map<java.lang.String, java.lang.String>)
      driverlocator      | 2018-11-29 13:50:01.265  INFO 1 --- [           main] s.b.a.e.w.s.WebMvcEndpointHandlerMapping : Mapped "{[/actuator/info],methods=[GET],produces=[application/vnd.spring-boot.actuator.v2+json || application/json]}" onto public java.lang.Object org.springframework.boot.actuate.endpoint.web.servlet.AbstractWebMvcEndpointHandlerMapping$OperationHandler.handle(javax.servlet.http.HttpServletRequest,java.util.Map<java.lang.String, java.lang.String>)
      driverlocator      | 2018-11-29 13:50:01.272  INFO 1 --- [           main] s.b.a.e.w.s.WebMvcEndpointHandlerMapping : Mapped "{[/actuator],methods=[GET],produces=[application/vnd.spring-boot.actuator.v2+json || application/json]}" onto protected java.util.Map<java.lang.String, java.util.Map<java.lang.String, org.springframework.boot.actuate.endpoint.web.Link>> org.springframework.boot.actuate.endpoint.web.servlet.WebMvcEndpointHandlerMapping.links(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)
      driverlocator      | 2018-11-29 13:50:01.998  INFO 1 --- [           main] org.mongodb.driver.cluster               : Cluster created with settings {hosts=[localhost:27017], mode=SINGLE, requiredClusterType=UNKNOWN, serverSelectionTimeout='30000 ms', maxWaitQueueSize=500}
      driverlocator      | 2018-11-29 13:50:02.103  INFO 1 --- [localhost:27017] org.mongodb.driver.cluster               : Exception in monitor thread while connecting to server localhost:27017
      driverlocator      | 2018-11-29 13:50:02.447  INFO 1 --- [           main] pertySourcedRequestMappingHandlerMapping : Mapped URL path [/v2/api-docs] onto method [public org.springframework.http.ResponseEntity<springfox.documentation.spring.web.json.Json> springfox.documentation.swagger2.web.Swagger2Controller.getDocumentation(java.lang.String,javax.servlet.http.HttpServletRequest)]
      driverlocator      | 2018-11-29 13:50:03.183  INFO 1 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/webjars/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
      driverlocator      | 2018-11-29 13:50:03.183  INFO 1 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
      driverlocator      | 2018-11-29 13:50:03.809  INFO 1 --- [           main] s.w.s.m.m.a.RequestMappingHandlerAdapter : Looking for @ControllerAdvice: org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext@4f4a7090: startup date [Thu Nov 29 13:49:46 UTC 2018]; root of context hierarchy
      driverlocator      | 2018-11-29 13:50:04.238  INFO 1 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**/favicon.ico] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
      driverlocator      | 2018-11-29 13:50:04.605  INFO 1 --- [           main] org.mongodb.driver.cluster               : Cluster created with settings {hosts=[localhost:27017], mode=SINGLE, requiredClusterType=UNKNOWN, serverSelectionTimeout='30000 ms', maxWaitQueueSize=500}
      driverlocator      | 2018-11-29 13:50:04.626  INFO 1 --- [localhost:27017] org.mongodb.driver.cluster               : Exception in monitor thread while connecting to server localhost:27017
      driverlocator      | 2018-11-29 13:50:05.579  INFO 1 --- [           main] o.s.s.c.ThreadPoolTaskScheduler          : Initializing ExecutorService  'taskScheduler'
      driverlocator      | 2018-11-29 13:50:06.138  INFO 1 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
      driverlocator      | 2018-11-29 13:50:06.167  INFO 1 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Bean with name 'integrationMbeanExporter' has been autodetected for JMX exposure
      driverlocator      | 2018-11-29 13:50:06.175  INFO 1 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Located managed bean 'integrationMbeanExporter': registering with JMX server as MBean [org.springframework.integration.monitor:name=integrationMbeanExporter,type=IntegrationMBeanExporter]
      driverlocator      | 2018-11-29 13:50:06.366  INFO 1 --- [           main] o.s.i.monitor.IntegrationMBeanExporter   : Registering beans for JMX exposure on startup
      driverlocator      | 2018-11-29 13:50:06.370  INFO 1 --- [           main] o.s.i.monitor.IntegrationMBeanExporter   : Registering MessageChannel errorChannel
      driverlocator      | 2018-11-29 13:50:06.382  INFO 1 --- [           main] o.s.i.monitor.IntegrationMBeanExporter   : Located managed bean 'org.springframework.integration:type=MessageChannel,name=errorChannel': registering with JMX server as MBean [org.springframework.integration:type=MessageChannel,name=errorChannel]
      driverlocator      | 2018-11-29 13:50:06.543  INFO 1 --- [           main] o.s.i.monitor.IntegrationMBeanExporter   : Registering MessageChannel nullChannel
      driverlocator      | 2018-11-29 13:50:06.550  INFO 1 --- [           main] o.s.i.monitor.IntegrationMBeanExporter   : Located managed bean 'org.springframework.integration:type=MessageChannel,name=nullChannel': registering with JMX server as MBean [org.springframework.integration:type=MessageChannel,name=nullChannel]
      driverlocator      | 2018-11-29 13:50:06.573  INFO 1 --- [           main] o.s.i.monitor.IntegrationMBeanExporter   : Registering MessageHandler errorLogger
      driverlocator      | 2018-11-29 13:50:06.577  INFO 1 --- [           main] o.s.i.monitor.IntegrationMBeanExporter   : Located managed bean 'org.springframework.integration:type=MessageHandler,name=errorLogger,bean=internal': registering with JMX server as MBean [org.springframework.integration:type=MessageHandler,name=errorLogger,bean=internal]
      driverlocator      | 2018-11-29 13:50:06.677  INFO 1 --- [           main] o.s.c.support.DefaultLifecycleProcessor  : Starting beans in phase 0
      driverlocator      | 2018-11-29 13:50:06.686  INFO 1 --- [           main] o.s.i.endpoint.EventDrivenConsumer       : Adding {logging-channel-adapter:_org.springframework.integration.errorLogger} as a subscriber to the 'errorChannel' channel
      driverlocator      | 2018-11-29 13:50:06.689  INFO 1 --- [           main] o.s.i.channel.PublishSubscribeChannel    : Channel 'Driver-Locator.errorChannel' has 1 subscriber(s).
      driverlocator      | 2018-11-29 13:50:06.691  INFO 1 --- [           main] o.s.i.endpoint.EventDrivenConsumer       : started _org.springframework.integration.errorLogger
      driverlocator      | 2018-11-29 13:50:06.692  INFO 1 --- [           main] o.s.c.support.DefaultLifecycleProcessor  : Starting beans in phase 2147483647
      driverlocator      | 2018-11-29 13:50:06.694  INFO 1 --- [           main] d.s.w.p.DocumentationPluginsBootstrapper : Context refreshed
      driverlocator      | 2018-11-29 13:50:06.798  INFO 1 --- [           main] d.s.w.p.DocumentationPluginsBootstrapper : Found 1 custom documentation plugin(s)
      driverlocator      | 2018-11-29 13:50:06.952  INFO 1 --- [           main] s.d.s.w.s.ApiListingReferenceScanner     : Scanning for api listing references
      driverlocator      | 2018-11-29 13:50:07.891  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8081 (http) with context path ''
      driverlocator      | 2018-11-29 13:50:07.900  INFO 1 --- [           main] c.d.DriverLocatorApplication             : Started DriverLocatorApplication in 23.916 seconds (JVM running for 25.908)

      driverlocator      | 2018-11-29 13:59:29.078  INFO 1 --- [nio-8081-exec-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring FrameworkServlet 'dispatcherServlet'
      driverlocator      | 2018-11-29 13:59:29.082  INFO 1 --- [nio-8081-exec-1] o.s.web.servlet.DispatcherServlet        : FrameworkServlet 'dispatcherServlet': initialization started
      driverlocator      | 2018-11-29 13:59:29.171  INFO 1 --- [nio-8081-exec-1] o.s.web.servlet.DispatcherServlet        : FrameworkServlet 'dispatcherServlet': initialization completed in 88 ms
    ```





